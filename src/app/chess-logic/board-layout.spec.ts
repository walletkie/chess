import { BoardLayout, Col, Row } from './board-layout';
import { ChessFigure } from './models/chess-figure.enum';
import { Coordinate } from './models/coordinate';
import { PlayerSide } from './models/player-side.enum';

describe('BoardLayout', () => {

  it('should move row forward', () => {
    const startRow: Row = '4';

    const row = BoardLayout.getRowPosition(startRow, 2);

    expect(row).toBe('6');
  });

  it('should move row backward', () => {
    const startRow: Row = '2';

    const row = BoardLayout.getRowPosition(startRow, -1);

    expect(row).toBe('1');
  });

  it('should move row out of border', () => {
    const startRow: Row = '8';

    const row = BoardLayout.getRowPosition(startRow, 1);

    expect(row).toBeNull();
  });

  it('should move col backward', () => {
    const startCol: Col = 'b';

    const row = BoardLayout.getColPosition(startCol, -1);

    expect(row).toBe('a');
  });

  it('should move col out of border', () => {
    const startCol: Col = 'd';

    const col = BoardLayout.getColPosition(startCol, -5);

    expect(col).toBeNull();
  });

  it('should encode move', () => {
    const fromCoordinate: Coordinate = { row: '2', col: 'b' };

    const toCoordinate: Coordinate = { row: '4', col: 'b' };

    expect(BoardLayout.encodeMove(fromCoordinate, toCoordinate)).toBe('b2-b4');
  });

  it('should encode move with promotion to queen', () => {
    const fromCoordinate: Coordinate = { row: '7', col: 'b' };

    const toCoordinate: Coordinate = { row: '8', col: 'b' };

    expect(BoardLayout.encodeMove(fromCoordinate, toCoordinate, ChessFigure.QUEEN)).toBe('b7-b8:5');
  });

  it('should retrn first row', () => {
    expect(BoardLayout.firstRow).toBe('1');
  });

  it('should retrn last row', () => {
    expect(BoardLayout.lastRow).toBe('8');
  });

  it('should encode coordinate', () => {
    const field: Coordinate = { row: '1', col: 'a' };

    const encodedCoordinate = BoardLayout.encodeCoordinate(field);

    expect(encodedCoordinate).toBe('a1');
  });

  it('should decode coordinate', () => {
    const coordinate = 'a1';

    const decodedCoordinate = BoardLayout.decodeCoordinate(coordinate);

    expect(decodedCoordinate.row).toBe('1');
    expect(decodedCoordinate.col).toBe('a');
  });

  it('should return oponent side', () => {
    expect(BoardLayout.getOponentSide(PlayerSide.WHITE)).toBe(PlayerSide.BLACK);
    expect(BoardLayout.getOponentSide(PlayerSide.BLACK)).toBe(PlayerSide.WHITE);
    expect(BoardLayout.getOponentSide(PlayerSide.NONE)).toBeNull();
  });

});
