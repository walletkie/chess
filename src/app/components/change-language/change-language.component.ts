import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { DefaultLanguage, Language } from 'src/app/models/language.enum';

@Component({
  selector: 'app-change-language',
  templateUrl: './change-language.component.html',
  styleUrls: ['./change-language.component.scss']
})
export class ChangeLanguageComponent implements OnInit {

  public Language = Language;
  public language: string;

  constructor(
    private cookieService: CookieService,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.language = this.cookieService.get('language') || DefaultLanguage;
  }

  onLanguageChange(value: string): void {
    this.cookieService.set('language', value);
    this.translate.use(value);
  }

}
