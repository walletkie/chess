import { BoardLayout } from './board-layout';
import { ChessLogic } from './chess-logic';
import { FiguresBoard } from './figures-board';
import { ChessFigure } from './models/chess-figure.enum';
import { Coordinate } from './models/coordinate';
import { MoveType } from './models/move-type.enum';
import { PlayerSide } from './models/player-side.enum';
import { PossibleMovesBoard } from './possible-moves-board';

describe('PossibleMovesBoard', () => {

  it('should create an instance', () => {
    const chessLogic = new ChessLogic({});
    expect(new PossibleMovesBoard(chessLogic)).toBeTruthy();
  });

  it('should create empty move type board', () => {
    const emptyMovesBoard = PossibleMovesBoard.getEmptyMoveTypeBoard();

    for (const row of BoardLayout.rows) {
      for (const col of BoardLayout.cols) {
        expect(emptyMovesBoard[row][col]).toBe(MoveType.NONE);
      }
    }
  });

  describe('together with ChessLogic', () => {

    it('should return possible move type for selected figure to specified field', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d5: { chessFigure: ChessFigure.ROCK, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const rock: Coordinate = { row: '5', col: 'd' };
      const destination: Coordinate = { row: '1', col: 'd' };

      chessLogic.clickedAt(rock);
      const moveType = chessLogic.possibleMovesBoard.get(destination);

      expect(moveType).toBeDefined();
      expect(moveType).toBe(MoveType.MOVE);
    });

    it('should update current possible moves for selected field', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        b7: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        h2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        g3: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const pawn: Coordinate = { row: '2', col: 'h' };

      chessLogic.clickedAt(pawn);
      const moveOne = chessLogic.possibleMovesBoard.current['3']['h'].getValue();
      const moveTwo = chessLogic.possibleMovesBoard.current['4']['h'].getValue();
      const moveThree = chessLogic.possibleMovesBoard.current['3']['g'].getValue();

      expect(moveOne).toBe(MoveType.MOVE);
      expect(moveTwo).toBe(MoveType.MOVE);
      expect(moveThree).toBe(MoveType.ATTACK);
    });

  });

});
