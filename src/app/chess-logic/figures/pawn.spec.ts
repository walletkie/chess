import { ChessLogic } from '../chess-logic';
import { FieldsHistoryBoard } from '../fields-history-board';
import { FiguresBoard } from '../figures-board';
import { ChessFigure } from '../models/chess-figure.enum';
import { Coordinate } from '../models/coordinate';
import { Figure } from '../models/figure';
import { Move } from '../models/move';
import { MoveType } from '../models/move-type.enum';
import { PlayerSide } from '../models/player-side.enum';
import { Pawn } from './pawn';

describe('Pawn', () => {
  it('should create an instance', () => {
    const chessLogic = new ChessLogic({});
    expect(new Pawn(chessLogic)).toBeTruthy();
  });

  it('should return valid direction', () => {
    const whitePawnFigure: Figure = {
      chessFigure: ChessFigure.PAWN,
      color: PlayerSide.WHITE
    };
    const blackPawnFigure: Figure = {
      chessFigure: ChessFigure.PAWN,
      color: PlayerSide.BLACK
    };
    const emptyFigure: Figure = {
      chessFigure: ChessFigure.NONE,
      color: PlayerSide.NONE
    };

    expect(Pawn.getPawnDirection(whitePawnFigure)).toBe(1);
    expect(Pawn.getPawnDirection(blackPawnFigure)).toBe(-1);
    expect(Pawn.getPawnDirection(emptyFigure)).toBeNull();
  });

  it('should return valid starting row', () => {
    const whitePawnFigure: Figure = {
      chessFigure: ChessFigure.PAWN,
      color: PlayerSide.WHITE
    };
    const blackPawnFigure: Figure = {
      chessFigure: ChessFigure.PAWN,
      color: PlayerSide.BLACK
    };
    const emptyFigure: Figure = {
      chessFigure: ChessFigure.NONE,
      color: PlayerSide.NONE
    };

    expect(Pawn.getStartRowFor(whitePawnFigure)).toBe('2');
    expect(Pawn.getStartRowFor(blackPawnFigure)).toBe('7');
    expect(Pawn.getStartRowFor(emptyFigure)).toBeNull();
  });

  it('should return valid promotion row', () => {
    const whitePawnFigure: Figure = {
      chessFigure: ChessFigure.PAWN,
      color: PlayerSide.WHITE
    };
    const blackPawnFigure: Figure = {
      chessFigure: ChessFigure.PAWN,
      color: PlayerSide.BLACK
    };
    const emptyFigure: Figure = {
      chessFigure: ChessFigure.NONE,
      color: PlayerSide.NONE
    };

    expect(Pawn.getPromotionRowFor(whitePawnFigure)).toBe('8');
    expect(Pawn.getPromotionRowFor(blackPawnFigure)).toBe('1');
    expect(Pawn.getPromotionRowFor(emptyFigure)).toBeNull();
  });

  it('should return value if move is two fields forward', () => {
    const twoFieldsForwardMove: Move = {
      from: {
        row: '2',
        col: 'a'
      },
      to: {
        row: '4',
        col: 'a'
      },
      type: MoveType.MOVE
    };

    const oneFieldForwardMove: Move = {
      from: {
        row: '2',
        col: 'b'
      },
      to: {
        row: '3',
        col: 'b'
      },
      type: MoveType.MOVE
    };

    expect(Pawn.isTwoFieldsForwardMove(twoFieldsForwardMove)).toBe(true);
    expect(Pawn.isTwoFieldsForwardMove(oneFieldForwardMove)).toBe(false);
  });

  describe('together with ChessLogic', () => {

    describe('move type', () => {

      it('should return move move type - one field forward', () => {
        const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
          e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
          e8: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
          g2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        });
        const chessLogic = new ChessLogic({
          figuresBoardSnapshot,
          playsAs: PlayerSide.WHITE
        });
        const position: Coordinate = { row: '2', col: 'g' };
        const destination: Coordinate = { row: '3', col: 'g' };

        const moveType = chessLogic.pawn.getMoveType(position, destination);

        expect(moveType).toBeDefined();
        expect(moveType).toBe(MoveType.MOVE);
      });

      it('should return move move type - two fields forward', () => {
        const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
          e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
          e8: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
          g2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        });
        const chessLogic = new ChessLogic({
          figuresBoardSnapshot,
          playsAs: PlayerSide.WHITE
        });
        const position: Coordinate = { row: '2', col: 'g' };
        const destination: Coordinate = { row: '4', col: 'g' };

        const moveType = chessLogic.pawn.getMoveType(position, destination);

        expect(moveType).toBeDefined();
        expect(moveType).toBe(MoveType.MOVE);
      });

      it('should return attack move type', () => {
        const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
          e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
          e8: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
          g2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
          h3: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
        });
        const chessLogic = new ChessLogic({
          figuresBoardSnapshot,
          playsAs: PlayerSide.WHITE
        });
        const position: Coordinate = { row: '2', col: 'g' };
        const destination: Coordinate = { row: '3', col: 'h' };

        const moveType = chessLogic.pawn.getMoveType(position, destination);

        expect(moveType).toBeDefined();
        expect(moveType).toBe(MoveType.ATTACK);
      });

      it('should return en passant move type', () => {
        const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
          e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
          e8: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
          g5: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
          h5: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
        });
        const fieldsHistoryBoardSnapshot = FieldsHistoryBoard.createSnapshotFrom({
          h5: { enPassantable: true, attacked: false, moved: true }
        });
        const chessLogic = new ChessLogic({
          figuresBoardSnapshot,
          fieldsHistoryBoardSnapshot,
          playsAs: PlayerSide.WHITE
        });
        const position: Coordinate = { row: '5', col: 'g' };
        const destination: Coordinate = { row: '6', col: 'h' };

        const moveType = chessLogic.pawn.getMoveType(position, destination);

        expect(moveType).toBeDefined();
        expect(moveType).toBe(MoveType.EN_PASSANT);
      });

      it('should return promotion move type - promotion by attack', () => {
        const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
          e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
          e8: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
          g7: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
          h8: { chessFigure: ChessFigure.ROCK, color: PlayerSide.BLACK },
        });
        const chessLogic = new ChessLogic({
          figuresBoardSnapshot,
          playsAs: PlayerSide.WHITE
        });
        const position: Coordinate = { row: '7', col: 'g' };
        const destination: Coordinate = { row: '8', col: 'h' };

        const moveType = chessLogic.pawn.getMoveType(position, destination);

        expect(moveType).toBeDefined();
        expect(moveType).toBe(MoveType.PROMOTION);
      });

      it('should return promotion move type - promotion by move', () => {
        const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
          e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
          e8: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
          g7: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        });
        const chessLogic = new ChessLogic({
          figuresBoardSnapshot,
          playsAs: PlayerSide.WHITE
        });
        const position: Coordinate = { row: '7', col: 'g' };
        const destination: Coordinate = { row: '8', col: 'g' };

        const moveType = chessLogic.pawn.getMoveType(position, destination);

        expect(moveType).toBeDefined();
        expect(moveType).toBe(MoveType.PROMOTION);
      });

    });

    describe('all possible moves', () => {

      it('should return all possible moves - move', () => {
        const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
          e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
          e8: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
          g2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        });
        const chessLogic = new ChessLogic({
          figuresBoardSnapshot,
          playsAs: PlayerSide.WHITE
        });
        const pawn: Coordinate = { row: '2', col: 'g' };

        const allPossibleMoves = chessLogic.pawn.getAllPossibleMoves(pawn);

        expect(allPossibleMoves).toBeDefined();
        expect(allPossibleMoves.length).toBe(2);
        expect(allPossibleMoves.every((e) => e.from.row === pawn.row && e.from.col === pawn.col)).toBeTruthy();
        expect(allPossibleMoves.every((e) => e.type === MoveType.MOVE)).toBeTruthy();
        expect(allPossibleMoves.some((e) => e.to.row === '3' && e.to.col === 'g')).toBeTruthy();
        expect(allPossibleMoves.some((e) => e.to.row === '4' && e.to.col === 'g')).toBeTruthy();
      });

      it('should return all possible moves - attack', () => {
        const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
          e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
          e8: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
          g2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
          h3: { chessFigure: ChessFigure.ROCK, color: PlayerSide.BLACK },
          g3: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
        });
        const chessLogic = new ChessLogic({
          figuresBoardSnapshot,
          playsAs: PlayerSide.WHITE
        });
        const pawn: Coordinate = { row: '2', col: 'g' };

        const allPossibleMoves = chessLogic.pawn.getAllPossibleMoves(pawn);

        expect(allPossibleMoves).toBeDefined();
        expect(allPossibleMoves.length).toBe(1);
        expect(allPossibleMoves.every((e) => e.from.row === pawn.row && e.from.col === pawn.col)).toBeTruthy();
        expect(allPossibleMoves.every((e) => e.type === MoveType.ATTACK)).toBeTruthy();
        expect(allPossibleMoves.some((e) => e.to.row === '3' && e.to.col === 'h')).toBeTruthy();
      });

      it('should return all possible moves - en passant and move', () => {
        const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
          e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
          e8: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
          g5: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
          h5: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
          g6: { chessFigure: ChessFigure.ROCK, color: PlayerSide.BLACK },
        });
        const fieldsHistoryBoardSnapshot = FieldsHistoryBoard.createSnapshotFrom({
          h5: { enPassantable: true, attacked: false, moved: true }
        });
        const chessLogic = new ChessLogic({
          figuresBoardSnapshot,
          fieldsHistoryBoardSnapshot,
          playsAs: PlayerSide.WHITE
        });
        const pawn: Coordinate = { row: '5', col: 'g' };

        const allPossibleMoves = chessLogic.pawn.getAllPossibleMoves(pawn);

        expect(allPossibleMoves).toBeDefined();
        expect(allPossibleMoves.length).toBe(1);
        expect(allPossibleMoves.every((e) => e.from.row === pawn.row && e.from.col === pawn.col)).toBeTruthy();
        expect(allPossibleMoves.every((e) => e.type === MoveType.EN_PASSANT)).toBeTruthy();
        expect(allPossibleMoves.some((e) => e.to.row === '6' && e.to.col === 'h')).toBeTruthy();
      });

      it('should return all possible moves - protect king from beeing attacked by attack', () => {
        const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
          e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
          e8: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
          f2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
          e3: { chessFigure: ChessFigure.ROCK, color: PlayerSide.BLACK },
        });
        const chessLogic = new ChessLogic({
          figuresBoardSnapshot,
          playsAs: PlayerSide.WHITE
        });
        const pawn: Coordinate = { row: '2', col: 'f' };

        const allPossibleMoves = chessLogic.pawn.getAllPossibleMoves(pawn);

        expect(allPossibleMoves).toBeDefined();
        expect(allPossibleMoves.length).toBe(1);
        expect(allPossibleMoves.every((e) => e.from.row === pawn.row && e.from.col === pawn.col)).toBeTruthy();
        expect(allPossibleMoves.some((e) => e.type === MoveType.ATTACK)).toBeTruthy();
        expect(allPossibleMoves.some((e) => e.to.row === '3' && e.to.col === 'e')).toBeTruthy();
      });

      it('should return all possible moves - protect king from beeing attacked by en passant', () => {
        const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
          g4: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
          e8: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
          g5: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
          h5: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
        });
        const fieldsHistoryBoardSnapshot = FieldsHistoryBoard.createSnapshotFrom({
          h5: { enPassantable: true, attacked: false, moved: true }
        });
        const chessLogic = new ChessLogic({
          figuresBoardSnapshot,
          fieldsHistoryBoardSnapshot,
          playsAs: PlayerSide.WHITE
        });
        const pawn: Coordinate = { row: '5', col: 'g' };

        const allPossibleMoves = chessLogic.pawn.getAllPossibleMoves(pawn);

        expect(allPossibleMoves).toBeDefined();
        expect(allPossibleMoves.length).toBe(1);
        expect(allPossibleMoves.every((e) => e.from.row === pawn.row && e.from.col === pawn.col)).toBeTruthy();
        expect(allPossibleMoves.some((e) => e.type === MoveType.EN_PASSANT)).toBeTruthy();
        expect(allPossibleMoves.some((e) => e.to.row === '6' && e.to.col === 'h')).toBeTruthy();
      });

      it('should return all possible moves - keep protecting king from beeing attacked', () => {
        const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
          e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
          e8: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
          f2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
          h4: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.BLACK },
        });
        const chessLogic = new ChessLogic({
          figuresBoardSnapshot,
          playsAs: PlayerSide.WHITE
        });
        const pawn: Coordinate = { row: '2', col: 'f' };

        const allPossibleMoves = chessLogic.pawn.getAllPossibleMoves(pawn);

        expect(allPossibleMoves).toBeDefined();
        expect(allPossibleMoves.length).toBe(0);
      });

      it('should return all possible moves - promotion by attack', () => {
        const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
          e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
          e8: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
          g7: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
          g8: { chessFigure: ChessFigure.BISHOP, color: PlayerSide.BLACK },
          h8: { chessFigure: ChessFigure.ROCK, color: PlayerSide.BLACK },
        });
        const chessLogic = new ChessLogic({
          figuresBoardSnapshot,
          playsAs: PlayerSide.WHITE
        });
        const pawn: Coordinate = { row: '7', col: 'g' };

        const allPossibleMoves = chessLogic.pawn.getAllPossibleMoves(pawn);

        expect(allPossibleMoves).toBeDefined();
        expect(allPossibleMoves.length).toBe(1);
        expect(allPossibleMoves.every((e) => e.from.row === pawn.row && e.from.col === pawn.col)).toBeTruthy();
        expect(allPossibleMoves.every((e) => e.type === MoveType.PROMOTION)).toBeTruthy();
        expect(allPossibleMoves.some((e) => e.to.row === '8' && e.to.col === 'h')).toBeTruthy();
      });

      it('should return all possible moves - promotion by move', () => {
        const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
          e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
          e8: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
          g7: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        });
        const chessLogic = new ChessLogic({
          figuresBoardSnapshot,
          playsAs: PlayerSide.WHITE
        });
        const pawn: Coordinate = { row: '7', col: 'g' };

        const allPossibleMoves = chessLogic.pawn.getAllPossibleMoves(pawn);

        expect(allPossibleMoves).toBeDefined();
        expect(allPossibleMoves.length).toBe(1);
        expect(allPossibleMoves.every((e) => e.from.row === pawn.row && e.from.col === pawn.col)).toBeTruthy();
        expect(allPossibleMoves.every((e) => e.type === MoveType.PROMOTION)).toBeTruthy();
        expect(allPossibleMoves.some((e) => e.to.row === '8' && e.to.col === 'g')).toBeTruthy();
      });

    });

    it('should return all attackable fields - white pawn', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        e8: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        g2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const pawn: Coordinate = { row: '2', col: 'g' };

      const allAttackableFields = chessLogic.pawn.getAllAttackableFields(pawn);

      expect(allAttackableFields).toBeDefined();
      expect(allAttackableFields.length).toBe(2);
      expect(allAttackableFields.some((e) => e.row === '3' && e.col === 'h')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '3' && e.col === 'f')).toBeTruthy();
    });

    it('should return all attackable fields - black pawn', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        e8: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        g2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.BLACK
      });
      const pawn: Coordinate = { row: '2', col: 'g' };

      const allAttackableFields = chessLogic.pawn.getAllAttackableFields(pawn);

      expect(allAttackableFields).toBeDefined();
      expect(allAttackableFields.length).toBe(2);
      expect(allAttackableFields.some((e) => e.row === '1' && e.col === 'h')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '1' && e.col === 'f')).toBeTruthy();
    });

  });

});
