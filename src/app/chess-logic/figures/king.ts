import { BoardLayout } from '../board-layout';
import { ChessLogic } from '../chess-logic';
import { ChessFigure } from '../models/chess-figure.enum';
import { Coordinate } from '../models/coordinate';
import { Move } from '../models/move';
import { MoveType } from '../models/move-type.enum';
import { PlayerSide } from '../models/player-side.enum';
import { FigureBase } from './figure-base';

export class King extends FigureBase {

    private static readonly AVAILABLE_MOVES_POSIOTNOS: {
        [name: string]: { row: number, col: number }
    } = {
            UP: { row: 1, col: 0 },
            UP_RIGHT: { row: 1, col: 1 },
            RIGHT: { row: 0, col: 1 },
            DOWN_RIGHT: { row: -1, col: 1 },
            DOWN: { row: -1, col: 0 },
            DOWN_LEFT: { row: -1, col: -1 },
            LEFT: { row: 0, col: -1 },
            LEFT_UP: { row: 1, col: -1 },
        };

    constructor(
        private chessLogic: ChessLogic
    ) {
        super();
    }

    public getAllPossibleMoves(forField: Coordinate): Array<Move> {
        const possibleMoves: Array<Move> = [];

        const possibleToMoveFields = this.getPossibleToMoveFields(forField);
        for (const move of possibleToMoveFields) {
            possibleMoves.push({
                from: forField,
                to: move,
                type: MoveType.MOVE
            });
        }

        const possibleToAttackFields = this.getPossibleToAttackFields(forField);
        for (const attack of possibleToAttackFields) {
            possibleMoves.push({
                from: forField,
                to: attack,
                type: MoveType.ATTACK
            });
        }

        const possibleToCastlingFields = this.getPossibleToCastlingFields(forField);
        for (const castling of possibleToCastlingFields) {
            possibleMoves.push({
                from: forField,
                to: castling,
                type: MoveType.CASTLING
            });
        }

        return possibleMoves;
    }

    public getPossibleToMoveFields(forField: Coordinate): Array<Coordinate> {
        const moveableFields: Array<Coordinate> = [];
        const king = this.chessLogic.figuresBoard.get(forField);
        for (const moveName in King.AVAILABLE_MOVES_POSIOTNOS) {
            const move = King.AVAILABLE_MOVES_POSIOTNOS[moveName];
            const row = BoardLayout.getRowPosition(forField.row, move.row);
            const col = BoardLayout.getColPosition(forField.col, move.col);
            if (row && col) {
                const moveCoordinate = { row, col };
                // Next double check of under attack field is becouse of
                // optimalisation. It is much faster to check current uder
                // attack board because it is not going to change. And only
                // then check if its going to change in the future.
                if (this.chessLogic.figuresBoard.isEmpty(moveCoordinate) &&
                    !this.chessLogic.underAttackBoard.get(moveCoordinate, king.color) &&
                    !this.chessLogic.underAttackBoard.willKingBeUnderAttackAfterMove(forField, moveCoordinate)) {
                    moveableFields.push(moveCoordinate);
                }
            }
        }
        return moveableFields;
    }

    public getPossibleToAttackFields(forField: Coordinate): Array<Coordinate> {
        const attackableFields: Array<Coordinate> = [];
        const king = this.chessLogic.figuresBoard.get(forField);
        const allAttackableFields = this.getAllAttackableFields(forField);
        for (const attack of allAttackableFields) {
            if (this.chessLogic.figuresBoard.isObtainedByOponent(this.chessLogic.playsAs, attack) &&
                !this.chessLogic.underAttackBoard.get(attack, king.color)) {
                attackableFields.push(attack);
            }
        }
        return attackableFields;
    }

    public getPossibleToCastlingFields(forField: Coordinate): Array<Coordinate> {
        const castlingableFields: Array<Coordinate> = [];
        const king = this.chessLogic.figuresBoard.get(forField);
        const kingMoved = this.chessLogic.fieldsHistoryBoard.didFieldChangeSinceGameHadStarted(forField);
        const kingUnderAttack = this.chessLogic.underAttackBoard.get(forField, king.color);
        const playersStatingRow = (this.chessLogic.playsAs === PlayerSide.WHITE) ? BoardLayout.firstRow : BoardLayout.lastRow;

        const leftRockWasMovedOrAttacked = this.chessLogic.fieldsHistoryBoard.didFieldChangeSinceGameHadStarted(
            { row: playersStatingRow, col: 'a' }
        );
        if (!kingMoved &&
            !kingUnderAttack &&
            !leftRockWasMovedOrAttacked &&
            !this.chessLogic.underAttackBoard.get({ row: playersStatingRow, col: 'd' }, king.color) &&
            this.chessLogic.figuresBoard.isEmpty({ row: playersStatingRow, col: 'd' }) &&
            !this.chessLogic.underAttackBoard.get({ row: playersStatingRow, col: 'c' }, king.color) &&
            this.chessLogic.figuresBoard.isEmpty({ row: playersStatingRow, col: 'c' })
        ) {
            castlingableFields.push({ row: playersStatingRow, col: 'c' });
        }

        const rightRockWasMovedOrAttacked = this.chessLogic.fieldsHistoryBoard.didFieldChangeSinceGameHadStarted(
            { row: playersStatingRow, col: 'h' }
        );
        if (!kingMoved &&
            !kingUnderAttack &&
            !rightRockWasMovedOrAttacked &&
            !this.chessLogic.underAttackBoard.get({ row: playersStatingRow, col: 'f' }, king.color) &&
            this.chessLogic.figuresBoard.isEmpty({ row: playersStatingRow, col: 'f' }) &&
            !this.chessLogic.underAttackBoard.get({ row: playersStatingRow, col: 'g' }, king.color) &&
            this.chessLogic.figuresBoard.isEmpty({ row: playersStatingRow, col: 'g' })
        ) {
            castlingableFields.push({ row: playersStatingRow, col: 'g' });
        }

        return castlingableFields;
    }

    public getAllAttackableFields(forField: Coordinate): Array<Coordinate> {
        const attackableFields: Array<Coordinate> = [];
        for (const moveName in King.AVAILABLE_MOVES_POSIOTNOS) {
            const move = King.AVAILABLE_MOVES_POSIOTNOS[moveName];
            const row = BoardLayout.getRowPosition(forField.row, move.row);
            const col = BoardLayout.getColPosition(forField.col, move.col);
            if (row && col) {
                attackableFields.push({ row, col });
            }
        }
        return attackableFields;
    }

    public getMoveType(fromCoordinate: Coordinate, toCoordinate: Coordinate): MoveType {
        const destinationField = this.chessLogic.figuresBoard.get(toCoordinate);
        if (destinationField.chessFigure !== ChessFigure.NONE) {
            return MoveType.ATTACK;
        } else {
            if (fromCoordinate.col === 'e' && (toCoordinate.col === 'c' || toCoordinate.col === 'g')) {
                return MoveType.CASTLING;
            } else {
                return MoveType.MOVE;
            }
        }
    }

}
