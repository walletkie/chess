import { BoardLayout } from './board-layout';
import { ChessLogic } from './chess-logic';
import { FiguresBoard } from './figures-board';
import { ChessFigure } from './models/chess-figure.enum';
import { ChessStatus } from './models/chess-status.enum';
import { Coordinate } from './models/coordinate';
import { EncodedMove } from './models/encoded-move';
import { Moves } from './models/moves';
import { PlayerSide } from './models/player-side.enum';

describe('ChessLogic', () => {

  it('should create an instance', () => {
    const chessLogic = new ChessLogic({});

    expect(chessLogic).toBeTruthy();
  });

  describe('registring moves', () => {

    it('should register first move', () => {
      const chessLogic = new ChessLogic({});
      const moves: Moves = {
        1: 'a2-a3',
      };

      chessLogic.updateMoves(moves);

      expect(Object.keys(chessLogic.movesHistory).length).toBe(Object.keys(moves).length);
      expect(chessLogic.movesHistory['1']).toBe(moves['1']);
    });

    it('should register only new moves', () => {
      const chessLogic = new ChessLogic({});
      const moves: Moves = {
        1: 'a2-a3',
        2: 'b2-b4'
      };
      const newMoves: Moves = {
        1: null,
        2: null,
        3: 'a3-a4'
      };

      chessLogic.updateMoves(moves);
      chessLogic.updateMoves(newMoves);

      expect(Object.keys(chessLogic.movesHistory).length).toBe(Object.keys(newMoves).length);
      expect(chessLogic.movesHistory['1']).toBe(moves['1']);
      expect(chessLogic.movesHistory['2']).toBe(moves['2']);
      expect(chessLogic.movesHistory['3']).toBe(newMoves['3']);
    });

  });

  describe('fields selection', () => {

    it('should select field', () => {
      const chessLogic = new ChessLogic({});
      const selected: Coordinate = { row: '2', col: 'b' };

      chessLogic.clickedAt(selected);

      expect(chessLogic.selected.getValue().row).toBe(selected.row);
      expect(chessLogic.selected.getValue().col).toBe(selected.col);
    });

    it('should not select empty field', () => {
      const chessLogic = new ChessLogic({});
      const selected: Coordinate = { row: '3', col: 'a' };

      chessLogic.clickedAt(selected);

      expect(chessLogic.selected.getValue()).toBeNull();
    });

    it('should select and deselect field', () => {
      const chessLogic = new ChessLogic({});
      const selected: Coordinate = { row: '2', col: 'b' };

      chessLogic.clickedAt(selected);
      chessLogic.clickedAt(selected);

      expect(chessLogic.selected.getValue()).toBeNull();
    });

    it('should not call callback function on selecting figure and then not available field', () => {
      const gameService = {
        callbackFunction: () => { }
      };
      spyOn(gameService, 'callbackFunction');
      const chessLogic = new ChessLogic({
        onMadeMove: gameService.callbackFunction
      });
      const selected: Coordinate = { row: '2', col: 'b' };
      const otherField: Coordinate = { row: '4', col: 'c' };

      chessLogic.clickedAt(selected);
      chessLogic.clickedAt(otherField);

      expect(gameService.callbackFunction).toHaveBeenCalledTimes(0);
      expect(chessLogic.selected.getValue().row).toBe(selected.row);
      expect(chessLogic.selected.getValue().col).toBe(selected.col);
    });

    it('should call callback function once and deselect field', () => {
      const gameService = {
        callbackFunction: () => { }
      };
      spyOn(gameService, 'callbackFunction');
      const chessLogic = new ChessLogic({
        onMadeMove: gameService.callbackFunction
      });
      const selected: Coordinate = { row: '2', col: 'b' };
      const destination: Coordinate = { row: '4', col: 'b' };

      chessLogic.clickedAt(selected);
      chessLogic.clickedAt(destination);

      expect(gameService.callbackFunction).toHaveBeenCalledTimes(1);
      expect(chessLogic.selected.getValue()).toBeNull();
    });

    it('should call server callback to make move with proper encoded move', () => {
      const selected: Coordinate = { row: '2', col: 'b' };
      const destination: Coordinate = { row: '4', col: 'b' };
      const move = BoardLayout.encodeMove(selected, destination);

      const serverMock = (moveId: number, encodedMove: EncodedMove) => {
        expect(moveId).toBe(3);
        expect(encodedMove).toBe(move);
      };
      const chessLogic = new ChessLogic({
        onMadeMove: serverMock
      });
      const endodedMoves: Moves = {
        1: 'a2-a4',
        2: 'd7-d6'
      };
      chessLogic.updateMoves(endodedMoves);

      chessLogic.clickedAt(selected);
      chessLogic.clickedAt(destination);

      expect(chessLogic.selected.getValue()).toBeNull();
    });

  });

  describe('updating moves', () => {

    it('should parse move and call callback that players turn changed', () => {
      const gameService = {
        onGameStatusChange: () => { }
      };
      spyOn(gameService, 'onGameStatusChange');
      const chessLogic = new ChessLogic({
        playsAs: PlayerSide.WHITE
      });
      const endodedMoves: Moves = {
        1: 'a2-a4'
      };

      chessLogic.status.subscribe(gameService.onGameStatusChange);
      chessLogic.updateMoves(endodedMoves);

      expect(gameService.onGameStatusChange).toHaveBeenCalledTimes(2);
      expect(chessLogic.figuresBoard.get({ row: '2', col: 'a' }).chessFigure).toBe(ChessFigure.NONE);
      expect(chessLogic.figuresBoard.get({ row: '2', col: 'a' }).color).toBe(PlayerSide.NONE);
      expect(chessLogic.figuresBoard.get({ row: '4', col: 'a' }).chessFigure).toBe(ChessFigure.PAWN);
      expect(chessLogic.figuresBoard.get({ row: '4', col: 'a' }).color).toBe(PlayerSide.WHITE);
    });

    it('should parse moves and call callback that players turn changed to oponent`s', () => {
      const chessLogic = new ChessLogic({
        playsAs: PlayerSide.WHITE
      });
      const endodedMoves1: Moves = {
        1: 'a2-a4',
        2: 'd7-d6',
      };
      const endodedMoves2: Moves = {
        1: 'a2-a4',
        2: 'd7-d6',
        3: 'a1-a3',
      };

      chessLogic.updateMoves(endodedMoves1);
      chessLogic.updateMoves(endodedMoves2);

      expect(chessLogic.status.getValue()).toBe(ChessStatus.BLACK_TURN);
      expect(chessLogic.figuresBoard.get({ row: '2', col: 'a' }).chessFigure).toBe(ChessFigure.NONE);
      expect(chessLogic.figuresBoard.get({ row: '2', col: 'a' }).color).toBe(PlayerSide.NONE);
      expect(chessLogic.figuresBoard.get({ row: '4', col: 'a' }).chessFigure).toBe(ChessFigure.PAWN);
      expect(chessLogic.figuresBoard.get({ row: '4', col: 'a' }).color).toBe(PlayerSide.WHITE);
      expect(chessLogic.figuresBoard.get({ row: '7', col: 'd' }).chessFigure).toBe(ChessFigure.NONE);
      expect(chessLogic.figuresBoard.get({ row: '7', col: 'd' }).color).toBe(PlayerSide.NONE);
      expect(chessLogic.figuresBoard.get({ row: '6', col: 'd' }).chessFigure).toBe(ChessFigure.PAWN);
      expect(chessLogic.figuresBoard.get({ row: '6', col: 'd' }).color).toBe(PlayerSide.BLACK);
      expect(chessLogic.figuresBoard.get({ row: '3', col: 'a' }).chessFigure).toBe(ChessFigure.ROCK);
      expect(chessLogic.figuresBoard.get({ row: '3', col: 'a' }).color).toBe(PlayerSide.WHITE);
    });

  });

  describe('game status', () => {

    it('should return status that is white players turn', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        f2: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });

      const status = chessLogic.status.getValue();

      expect(status).toBe(ChessStatus.WHITE_TURN);
    });

    it('should return status that is black players turn', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        e1: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        b2: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.BLACK
      });
      chessLogic.updateMoves({
        1: 'h2-f2'
      });

      const status = chessLogic.status.getValue();

      expect(status).toBe(ChessStatus.BLACK_TURN);
    });

    it('should return status that game is lost', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        f2: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.BLACK },
        d2: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });

      const status = chessLogic.status.getValue();

      expect(status).toBe(ChessStatus.WHITE_LOST);
    });

    it('should return status that game awaits for server response', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        e1: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        b2: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.BLACK
      });
      chessLogic.updateMoves({
        1: 'h2-f2'
      });

      chessLogic.clickedAt({ row: '1', col: 'e' });
      chessLogic.clickedAt({ row: '2', col: 'f' });
      const status = chessLogic.status.getValue();

      expect(status).toBe(ChessStatus.AWAITS);
    });

    it('should return status that game is draw', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        f2: { chessFigure: ChessFigure.ROCK, color: PlayerSide.BLACK },
        d2: { chessFigure: ChessFigure.ROCK, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });

      const status = chessLogic.status.getValue();

      expect(status).toBe(ChessStatus.DRAW);
    });

    it('should change status once that game is lost', () => {
      const gameService = {
        onGameStatusChange: () => { },
      };
      spyOn(gameService, 'onGameStatusChange');
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        f2: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.BLACK },
        d2: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });

      chessLogic.status.subscribe(gameService.onGameStatusChange);
      const status = chessLogic.status.getValue();

      expect(gameService.onGameStatusChange).toHaveBeenCalledTimes(1);
      expect(status).toBe(ChessStatus.WHITE_LOST);
    });

    it('should change status once that game is draw', () => {
      const gameService = {
        onGameStatusChange: () => { },
      };
      spyOn(gameService, 'onGameStatusChange');
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        f2: { chessFigure: ChessFigure.ROCK, color: PlayerSide.BLACK },
        d2: { chessFigure: ChessFigure.ROCK, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });

      chessLogic.status.subscribe(gameService.onGameStatusChange);
      const status = chessLogic.status.getValue();

      expect(gameService.onGameStatusChange).toHaveBeenCalledTimes(1);
      expect(status).toBe(ChessStatus.DRAW);
    });

  });

  describe('beaten figures list', () => {

    it('should add white figure to beaten list', () => {
      const gameService = {
        onGameStatusChange: () => { },
      };
      spyOn(gameService, 'onGameStatusChange');
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        f2: { chessFigure: ChessFigure.ROCK, color: PlayerSide.WHITE },
        d2: { chessFigure: ChessFigure.ROCK, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      chessLogic.updateMoves({
        1: 'd2-f2'
      });

      chessLogic.whiteBeatenFigures.subscribe(gameService.onGameStatusChange);
      const whiteBeatenFigures = chessLogic.whiteBeatenFigures.getValue();

      expect(gameService.onGameStatusChange).toHaveBeenCalledTimes(1);
      expect(whiteBeatenFigures).toBeDefined();
      expect(whiteBeatenFigures.length).toBe(1);
      expect(whiteBeatenFigures[0]).toBe(ChessFigure.ROCK);
    });

    it('should add black figure to beaten list', () => {
      const gameService = {
        onGameStatusChange: () => { },
      };
      spyOn(gameService, 'onGameStatusChange');
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        f2: { chessFigure: ChessFigure.ROCK, color: PlayerSide.WHITE },
        d2: { chessFigure: ChessFigure.ROCK, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      chessLogic.updateMoves({
        1: 'f2-d2'
      });

      chessLogic.blackBeatenFigures.subscribe(gameService.onGameStatusChange);
      const blackBeatenFigures = chessLogic.blackBeatenFigures.getValue();

      expect(gameService.onGameStatusChange).toHaveBeenCalledTimes(1);
      expect(blackBeatenFigures).toBeDefined();
      expect(blackBeatenFigures.length).toBe(1);
      expect(blackBeatenFigures[0]).toBe(ChessFigure.ROCK);
    });

  });

});
