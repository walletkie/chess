import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { faUserAltSlash } from '@fortawesome/free-solid-svg-icons';
import { Language } from 'src/app/models/language.enum';
import { AuthService } from 'src/app/services/auth.service';
import { PlayerService, Settings } from 'src/app/services/player.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  public faUserAltSlash = faUserAltSlash;
  public Language = Language;

  public editedSettings: Settings;
  public editedUsername: string;

  constructor(
    private dialogRef: MatDialogRef<SettingsComponent>,
    public authService: AuthService,
    private playerService: PlayerService,
    private router: Router,
  ) { }

  ngOnInit() {
    const currentSettings = this.playerService.settings.getValue();
    this.editedSettings = {
      language: currentSettings.language,
      sound: currentSettings.sound,
      vibrations: currentSettings.vibrations,
      highlightPossibleMoves: currentSettings.highlightPossibleMoves,
    };
    this.editedUsername = this.playerService.player.getValue().displayedName;
  }

  async onLogout(): Promise<void> {
    await this.authService.onLogout();
    this.onCancel();
    this.router.navigateByUrl('/');
  }

  onCancel() {
    this.dialogRef.close();
  }

  public getDiffrences(baseValue: Settings, newValue: Settings): Partial<Settings> {
    const diffrence: Partial<Settings> = {};

    if (baseValue.language !== newValue.language) {
      diffrence.language = newValue.language;
    }

    if (baseValue.sound !== newValue.sound) {
      diffrence.sound = newValue.sound;
    }

    if (baseValue.vibrations !== newValue.vibrations) {
      diffrence.vibrations = newValue.vibrations;
    }

    if (baseValue.highlightPossibleMoves !== newValue.highlightPossibleMoves) {
      diffrence.highlightPossibleMoves = newValue.highlightPossibleMoves;
    }

    return diffrence;
  }

  onSave() {
    let username = (this.editedUsername) ? this.editedUsername.trim() : '';
    username = (username === '') ? null : username;
    if (username !== this.playerService.player.getValue().displayedName) {
      this.playerService.updatePlayer({ displayedName: username });
    }

    const settingsDiffrence = this.getDiffrences(this.playerService.settings.getValue(), this.editedSettings);
    if (Object.keys(settingsDiffrence).length > 0) {
      this.playerService.updateSettings(settingsDiffrence);
    }

    this.dialogRef.close();
  }

}
