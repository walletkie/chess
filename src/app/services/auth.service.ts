import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { BehaviorSubject } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public readonly isReady = new BehaviorSubject<boolean>(false);
  public readonly user = new BehaviorSubject<User>({
    isLoggedIn: false
  });

  constructor(
    private fireAuth: AngularFireAuth
  ) {
    this.fireAuth.authState.subscribe((user) => {
      this.isReady.next(true);
      if (user) {
        this.loggedWithUser(user);
      } else {
        this.user.next({
          isLoggedIn: false
        });
      }
    });
  }

  private async loggedWithUser(user: firebase.User): Promise<void> {
    const userData: User = {
      isLoggedIn: true,
      uid: user.uid,
      isAnonymus: user.isAnonymous
    };
    if (!user.isAnonymous) {
      userData.email = user.email;
      userData.avatarUrl = user.photoURL;
      userData.displayedName = user.displayName;
    }
    this.user.next(userData);
  }

  onLoginWithGoogle(): void {
    const provider = new auth.GoogleAuthProvider();
    this.fireAuth.auth.signInWithPopup(provider)
      .catch((error) => {
        console.error(error);
      });
  }

  onLoginWithFacebook(): void {
    const provider = new auth.FacebookAuthProvider();
    this.fireAuth.auth.signInWithPopup(provider)
      .catch((error) => {
        console.error(error);
      });
  }

  onLoginWithAnonymously(): void {
    this.fireAuth.auth.signInAnonymously()
      .catch((error) => {
        console.error(error);
      });
  }

  async onLogout(): Promise<void> {
    if (this.fireAuth.auth.currentUser) {
      await this.fireAuth.auth.signOut();
    }
    this.user.next({
      isLoggedIn: false
    });
    this.isReady.next(true);
  }
}
