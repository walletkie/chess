import { ChessFigure } from './chess-figure.enum';
import { PlayerSide } from './player-side.enum';

export interface Figure {
    chessFigure: ChessFigure;
    color: PlayerSide;
}
