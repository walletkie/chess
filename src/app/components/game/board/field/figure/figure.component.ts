import { Component, Input, OnChanges, OnInit } from '@angular/core';
import {
  faChessBishop, faChessKing, faChessKnight, faChessPawn,
  faChessQueen, faChessRook, IconDefinition
} from '@fortawesome/free-solid-svg-icons';
import { ChessFigure } from 'src/app/chess-logic/models/chess-figure.enum';
import { PlayerSide } from 'src/app/chess-logic/models/player-side.enum';

@Component({
  selector: 'app-figure',
  templateUrl: './figure.component.html',
  styleUrls: ['./figure.component.scss']
})
export class FigureComponent implements OnInit, OnChanges {

  public PlayerSide = PlayerSide;

  @Input()
  public chessFigure: ChessFigure;

  @Input()
  public color: PlayerSide;

  public figureIcon: IconDefinition;

  constructor() { }

  ngOnInit() { }

  ngOnChanges(): void {
    switch (this.chessFigure) {

      case ChessFigure.NONE:
        this.figureIcon = null;
        break;

      case ChessFigure.PAWN:
        this.figureIcon = faChessPawn;
        break;

      case ChessFigure.BISHOP:
        this.figureIcon = faChessBishop;
        break;

      case ChessFigure.KNIGHT:
        this.figureIcon = faChessKnight;
        break;

      case ChessFigure.QUEEN:
        this.figureIcon = faChessQueen;
        break;

      case ChessFigure.ROCK:
        this.figureIcon = faChessRook;
        break;

      case ChessFigure.KING:
        this.figureIcon = faChessKing;
        break;

      default:
        throw new Error('Unhandled chess figure to display');
    }
  }

}
