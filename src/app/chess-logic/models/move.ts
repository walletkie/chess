import { ChessFigure } from './chess-figure.enum';
import { Coordinate } from './coordinate';
import { MoveType } from './move-type.enum';

export interface Move {
    from: Coordinate;
    to: Coordinate;
    type: MoveType;
    promotion?: ChessFigure;
}
