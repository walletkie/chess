import { ChessLogic } from '../chess-logic';
import { FiguresBoard } from '../figures-board';
import { ChessFigure } from '../models/chess-figure.enum';
import { Coordinate } from '../models/coordinate';
import { MoveType } from '../models/move-type.enum';
import { PlayerSide } from '../models/player-side.enum';
import { Knight } from './knight';

describe('Knight', () => {

  it('should create an instance', () => {
    const chessLogic = new ChessLogic({});
    expect(new Knight(chessLogic)).toBeTruthy();
  });

  describe('together with ChessLogic', () => {

    it('should return all attackable fields', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d4: { chessFigure: ChessFigure.KNIGHT, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const knight: Coordinate = { row: '4', col: 'd' };

      const allAttackableFields = chessLogic.knight.getAllAttackableFields(knight);

      expect(allAttackableFields).toBeDefined();
      expect(allAttackableFields.length).toBe(8);
      expect(allAttackableFields.some((e) => e.row === '6' && e.col === 'c')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '6' && e.col === 'e')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '5' && e.col === 'b')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '5' && e.col === 'f')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '3' && e.col === 'b')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '3' && e.col === 'f')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '2' && e.col === 'c')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '2' && e.col === 'e')).toBeTruthy();
    });

    it('should return possible to attack fields', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d4: { chessFigure: ChessFigure.KNIGHT, color: PlayerSide.WHITE },
        c6: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        e6: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const knight: Coordinate = { row: '4', col: 'd' };

      const possibleToAttackFields = chessLogic.knight.getPossibleToAttackFields(knight);

      expect(possibleToAttackFields).toBeDefined();
      expect(possibleToAttackFields.length).toBe(1);
      expect(possibleToAttackFields.some((e) => e.row === '6' && e.col === 'e')).toBeTruthy();
    });

    it('should return possible to attack fields when king is under attack', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        f5: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d4: { chessFigure: ChessFigure.KNIGHT, color: PlayerSide.WHITE },
        c6: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
        e6: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const knight: Coordinate = { row: '4', col: 'd' };

      const possibleToAttackFields = chessLogic.knight.getPossibleToAttackFields(knight);

      expect(possibleToAttackFields).toBeDefined();
      expect(possibleToAttackFields.length).toBe(1);
      expect(possibleToAttackFields.some((e) => e.row === '6' && e.col === 'e')).toBeTruthy();
    });

    it('should return possible to move fields when some path are blocked', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d4: { chessFigure: ChessFigure.KNIGHT, color: PlayerSide.WHITE },
        c6: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
        e6: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const knight: Coordinate = { row: '4', col: 'd' };

      const possibleToMoveFields = chessLogic.knight.getPossibleToMoveFields(knight);

      expect(possibleToMoveFields).toBeDefined();
      expect(possibleToMoveFields.length).toBe(6);
      expect(possibleToMoveFields.some((e) => e.row === '5' && e.col === 'b')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '5' && e.col === 'f')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '3' && e.col === 'b')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '3' && e.col === 'f')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '2' && e.col === 'c')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '2' && e.col === 'e')).toBeTruthy();
    });

    it('should return possible to move fields to block attacker from checking king', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        d6: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d4: { chessFigure: ChessFigure.KNIGHT, color: PlayerSide.WHITE },
        f6: { chessFigure: ChessFigure.ROCK, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const knight: Coordinate = { row: '4', col: 'd' };

      const possibleToMoveFields = chessLogic.knight.getPossibleToMoveFields(knight);

      expect(possibleToMoveFields).toBeDefined();
      expect(possibleToMoveFields.length).toBe(1);
      expect(possibleToMoveFields.some((e) => e.row === '6' && e.col === 'e')).toBeTruthy();
    });

    it('should return all possible moves which means blocking enemy queen or attacking it', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        d7: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d4: { chessFigure: ChessFigure.KNIGHT, color: PlayerSide.WHITE },
        f5: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const knight: Coordinate = { row: '4', col: 'd' };

      const allPossibleMoves = chessLogic.knight.getAllPossibleMoves(knight);

      expect(allPossibleMoves).toBeDefined();
      expect(allPossibleMoves.length).toBe(2);
      expect(allPossibleMoves.every((e) => e.from.row === '4' && e.from.col === 'd')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.type === MoveType.MOVE && e.to.row === '6' && e.to.col === 'e')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.type === MoveType.ATTACK && e.to.row === '5' && e.to.col === 'f')).toBeTruthy();
    });

    it('should return move move type', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d4: { chessFigure: ChessFigure.KNIGHT, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const destination: Coordinate = { row: '6', col: 'e' };

      const moveType = chessLogic.knight.getMoveType(destination);

      expect(moveType).toBeDefined();
      expect(moveType).toBe(MoveType.MOVE);
    });

    it('should return attack move type', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d4: { chessFigure: ChessFigure.KNIGHT, color: PlayerSide.WHITE },
        f5: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const destination: Coordinate = { row: '5', col: 'f' };

      const moveType = chessLogic.knight.getMoveType(destination);

      expect(moveType).toBeDefined();
      expect(moveType).toBe(MoveType.ATTACK);
    });

  });

});
