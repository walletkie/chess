import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BoardLayout, Col, Row } from 'src/app/chess-logic/board-layout';
import { ChessLogic } from 'src/app/chess-logic/chess-logic';
import { ChessFigure } from 'src/app/chess-logic/models/chess-figure.enum';
import { PlayerSide } from 'src/app/chess-logic/models/player-side.enum';
import { SelectErrorCode } from 'src/app/chess-logic/models/select-error-code.enum';
import { GameStatus } from 'src/app/models/game-status.enum';
import { GameService } from 'src/app/services/game.service';
import { ChoosePromotionFigureComponent } from '../choose-promotion-figure/choose-promotion-figure.component';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {

  public PlayerSide = PlayerSide;
  public BoardLayout = BoardLayout;

  @Input()
  public chessLogic: ChessLogic;

  @Input()
  public playsAs: PlayerSide;

  constructor(
    private dialog: MatDialog,
    private gameService: GameService
  ) { }

  ngOnInit() { }

  private openPromotionDialog(row: Row, col: Col): void {
    const dialogRef = this.dialog.open(ChoosePromotionFigureComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.clickedAt(row, col, result as ChessFigure);
      }
    });
  }

  clickedAt(row: Row, col: Col, chessFigure?: ChessFigure): void {
    if (this.gameService.game.getValue().result !== GameStatus.UNDEFINED) {
      return;
    }

    try {
      this.chessLogic.clickedAt({ row, col }, chessFigure);
    } catch (error) {
      const selectError = error as SelectErrorCode;
      if (selectError === SelectErrorCode.NOT_PLAYERS_TURN) {
        // Intentionaly left empty
      } else if (selectError === SelectErrorCode.SELECTED_OPONENTS_FIGURE) {
        // Intentionaly left empty
      } else if (selectError === SelectErrorCode.GAME_HAS_ENDED) {
        // Intentionaly left empty
      } else if (selectError === SelectErrorCode.PROMOTION_FIGURE_NOT_PROVIDED) {
        this.openPromotionDialog(row, col);
      } else {
        throw error;
      }
    }
  }

}
