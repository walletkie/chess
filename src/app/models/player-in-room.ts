import { Player } from './player';
import { PlayerStatus } from './player-status.enum';

export interface PlayerInRoom extends Player {
    status: PlayerStatus;
}
