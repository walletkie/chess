export enum DisplayedStatus {
    NONE,
    AWAITS,
    TURN,
    IS_CHECKED,
    LOST,
    WON,
    DRAW,
    SURRENDERED
}
