export interface FieldHistory {
    moved: boolean;
    attacked: boolean;
    enPassantable: boolean;
}
