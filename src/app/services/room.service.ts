import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { BehaviorSubject, Subscription } from 'rxjs';
import { PlayerStatus } from '../models/player-status.enum';
import { getNewRoom, Room } from '../models/room';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  public readonly roomId = new BehaviorSubject<string>(undefined);
  public readonly room = new BehaviorSubject<Room>(undefined);
  public readonly isRoomOwner = new BehaviorSubject<boolean>(undefined);

  private roomDocument: AngularFirestoreDocument<Room>;
  private roomDocumentSubscription: Subscription;

  constructor(
    private firestore: AngularFirestore,
    private authService: AuthService,
  ) {
    this.roomId.subscribe((roomId) => this.onRoomIdChange(roomId));
  }

  public changeRoomId(roomId: string): void {
    if (this.roomId.getValue() === roomId) {
      return;
    }
    this.roomId.next(roomId);
  }

  private onRoomIdChange(roomId: string): void {
    if (this.roomDocumentSubscription) {
      this.roomDocumentSubscription.unsubscribe();
    }
    this.room.next(undefined);
    this.roomDocument = undefined;
    this.isRoomOwner.next(!!roomId && this.authService.user.getValue().uid === roomId);

    if (roomId) {
      this.roomDocument = this.firestore.collection('rooms').doc<Room>(roomId);
      this.roomDocumentSubscription = this.roomDocument.valueChanges().subscribe((roomData) => this.onRoomChange(roomData));
    }
  }

  private onRoomChange(roomData: Room): void {
    if (!roomData && this.isRoomOwner.getValue()) {
      this.roomDocument.set(
        getNewRoom(this.authService.user.getValue().uid)
      );
    } else if (roomData) {
      this.room.next(roomData);
      this.registerNewPlayer();
    } else {
      this.room.next(null);
    }
  }

  private registerNewPlayer(): void {
    const uid = this.authService.user.getValue().uid;
    if (!(uid in this.room.getValue().players)) {
      this.roomDocument.update({
        [`players.${uid}`]: PlayerStatus.ACTIVE
      });
    }
  }

  public async updateGameIdInRoom(gameId: string): Promise<void> {
    const roomData = {
      game: gameId
    };
    await this.roomDocument.update(roomData);
  }

}
