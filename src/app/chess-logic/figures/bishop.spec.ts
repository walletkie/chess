import { ChessLogic } from '../chess-logic';
import { FiguresBoard } from '../figures-board';
import { ChessFigure } from '../models/chess-figure.enum';
import { Coordinate } from '../models/coordinate';
import { MoveType } from '../models/move-type.enum';
import { PlayerSide } from '../models/player-side.enum';
import { Bishop } from './bishop';

describe('Bishop', () => {

  it('should create an instance', () => {
    const chessLogic = new ChessLogic({});
    expect(new Bishop(chessLogic)).toBeTruthy();
  });

  describe('together with ChessLogic', () => {

    it('should return all attackable fields when some path are blocked', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d5: { chessFigure: ChessFigure.BISHOP, color: PlayerSide.WHITE },
        b7: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        b3: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        f7: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        f3: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const bishop: Coordinate = { row: '5', col: 'd' };

      const allAttackableFields = chessLogic.bishop.getAllAttackableFields(bishop);

      expect(allAttackableFields).toBeDefined();
      expect(allAttackableFields.length).toBe(8);
      expect(allAttackableFields.some((e) => e.row === '7' && e.col === 'b')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '3' && e.col === 'b')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '6' && e.col === 'c')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '4' && e.col === 'c')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '6' && e.col === 'e')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '4' && e.col === 'e')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '7' && e.col === 'f')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '3' && e.col === 'f')).toBeTruthy();
    });

    it('should return possible to attack fields when some path are blocked', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d5: { chessFigure: ChessFigure.BISHOP, color: PlayerSide.WHITE },
        c4: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        b3: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
        f7: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const bishop: Coordinate = { row: '5', col: 'd' };

      const possibleToAttackFields = chessLogic.bishop.getPossibleToAttackFields(bishop);

      expect(possibleToAttackFields).toBeDefined();
      expect(possibleToAttackFields.length).toBe(1);
      expect(possibleToAttackFields.some((e) => e.row === '7' && e.col === 'f')).toBeTruthy();
    });

    it('should return possible to attack fields when king is under attack', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a2: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d5: { chessFigure: ChessFigure.BISHOP, color: PlayerSide.WHITE },
        c6: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
        g2: { chessFigure: ChessFigure.ROCK, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const bishop: Coordinate = { row: '5', col: 'd' };

      const possibleToAttackFields = chessLogic.bishop.getPossibleToAttackFields(bishop);

      expect(possibleToAttackFields).toBeDefined();
      expect(possibleToAttackFields.length).toBe(1);
      expect(possibleToAttackFields.some((e) => e.row === '2' && e.col === 'g')).toBeTruthy();
    });

    it('should return possible to move fields when some path are blocked', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d5: { chessFigure: ChessFigure.BISHOP, color: PlayerSide.WHITE },
        f7: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        b3: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        f3: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const bishop: Coordinate = { row: '5', col: 'd' };

      const possibleToMoveFields = chessLogic.bishop.getPossibleToMoveFields(bishop);

      expect(possibleToMoveFields).toBeDefined();
      expect(possibleToMoveFields.length).toBe(6);
      expect(possibleToMoveFields.some((e) => e.row === '8' && e.col === 'a')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '7' && e.col === 'b')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '6' && e.col === 'c')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '6' && e.col === 'e')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '4' && e.col === 'e')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '4' && e.col === 'c')).toBeTruthy();
    });

    it('should return possible to move fields to block attacker from checking king', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        c3: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d5: { chessFigure: ChessFigure.BISHOP, color: PlayerSide.WHITE },
        g3: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const bishop: Coordinate = { row: '5', col: 'd' };

      const possibleToMoveFields = chessLogic.bishop.getPossibleToMoveFields(bishop);

      expect(possibleToMoveFields).toBeDefined();
      expect(possibleToMoveFields.length).toBe(1);
      expect(possibleToMoveFields.some((e) => e.row === '3' && e.col === 'f')).toBeTruthy();
    });

    it('should return possible to move fields keep king from beeing check', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        c4: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        d5: { chessFigure: ChessFigure.BISHOP, color: PlayerSide.BLACK },
        f7: { chessFigure: ChessFigure.BISHOP, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.BLACK
      });
      const bishop: Coordinate = { row: '5', col: 'd' };

      const possibleToMoveFields = chessLogic.bishop.getPossibleToMoveFields(bishop);

      expect(possibleToMoveFields).toBeDefined();
      expect(possibleToMoveFields.length).toBe(1);
      expect(possibleToMoveFields.some((e) => e.row === '6' && e.col === 'e')).toBeTruthy();
    });

    it('should return all possible moves which means blocking enemy queen or attacking it', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a3: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d5: { chessFigure: ChessFigure.BISHOP, color: PlayerSide.WHITE },
        f3: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const bishop: Coordinate = { row: '5', col: 'd' };

      const allPossibleMoves = chessLogic.bishop.getAllPossibleMoves(bishop);

      expect(allPossibleMoves).toBeDefined();
      expect(allPossibleMoves.length).toBe(2);
      expect(allPossibleMoves.every((e) => e.from.row === '5' && e.from.col === 'd')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.type === MoveType.MOVE && e.to.row === '3' && e.to.col === 'b')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.type === MoveType.ATTACK && e.to.row === '3' && e.to.col === 'f')).toBeTruthy();
    });

    it('should return move move type', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d5: { chessFigure: ChessFigure.BISHOP, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const destination: Coordinate = { row: '4', col: 'e' };

      const moveType = chessLogic.bishop.getMoveType(destination);

      expect(moveType).toBeDefined();
      expect(moveType).toBe(MoveType.MOVE);
    });

    it('should return attack move type', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d5: { chessFigure: ChessFigure.BISHOP, color: PlayerSide.WHITE },
        e4: { chessFigure: ChessFigure.BISHOP, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const destination: Coordinate = { row: '4', col: 'e' };

      const moveType = chessLogic.bishop.getMoveType(destination);

      expect(moveType).toBeDefined();
      expect(moveType).toBe(MoveType.ATTACK);
    });

  });

});
