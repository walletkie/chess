export interface Board<T> {
    [row: string]: {
        [col: string]: T;
    };
}
