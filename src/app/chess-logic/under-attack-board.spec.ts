import { BoardLayout } from './board-layout';
import { ChessLogic } from './chess-logic';
import { FiguresBoard } from './figures-board';
import { ChessFigure } from './models/chess-figure.enum';
import { Coordinate } from './models/coordinate';
import { PlayerSide } from './models/player-side.enum';
import { UnderAttackBoard } from './under-attack-board';

describe('UnderAttackBoard', () => {

  it('should create an instance', () => {
    const chessLogic = new ChessLogic({});

    const underAttackBoard = new UnderAttackBoard(chessLogic);

    expect(underAttackBoard).toBeTruthy();
  });

  it('should return under attack field value', () => {
    const chessLogic = new ChessLogic({});
    const underAttackBoard = new UnderAttackBoard(chessLogic);
    const field: Coordinate = { row: '1', col: 'a' };

    const returnedValue = underAttackBoard.get(field, PlayerSide.WHITE);

    expect(returnedValue).toBeDefined();
    expect(returnedValue).toBeFalsy();
  });

  it('should throw expection on getting value out of board', () => {
    const chessLogic = new ChessLogic({});
    const underAttackBoard = new UnderAttackBoard(chessLogic);
    const field = { row: '9', col: null };

    const outOfBoardUderAttackField = () => underAttackBoard.get(field as Coordinate, PlayerSide.WHITE);

    expect(outOfBoardUderAttackField).toThrowError();
  });

  describe('together with ChessLogic', () => {

    it('should update board', () => {
      const chessLogic = new ChessLogic({});

      for (const col of BoardLayout.cols) {
        const field: Coordinate = {
          row: '6',
          col
        };
        expect(chessLogic.underAttackBoard.get(field, PlayerSide.WHITE)).toBeTruthy();
      }
    });

    it('should king will no longer be under attack after killing checking enemy', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        f2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
        e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        g1: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        underAttackTestingInstance: true,
        playsAs: PlayerSide.WHITE
      });
      const moveFrom: Coordinate = { row: '1', col: 'g' };
      const moveTo: Coordinate = { row: '2', col: 'f' };

      const uncheckingMove = chessLogic.underAttackBoard.willKingBeUnderAttackAfterMove(moveFrom, moveTo);

      expect(uncheckingMove).toBeFalsy();
    });

    it('should king still be under attack after moving pawn', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        f2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
        e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        g1: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        underAttackTestingInstance: true,
        playsAs: PlayerSide.WHITE
      });
      const moveFrom: Coordinate = { row: '1', col: 'g' };
      const moveTo: Coordinate = { row: '2', col: 'g' };

      const uncheckingMove = chessLogic.underAttackBoard.willKingBeUnderAttackAfterMove(moveFrom, moveTo);

      expect(uncheckingMove).toBeTruthy();
    });

    it('should king no longer be under attack after escaping', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        e7: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        d6: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        underAttackTestingInstance: true,
        playsAs: PlayerSide.BLACK
      });
      const moveFrom: Coordinate = { row: '7', col: 'e' };
      const moveTo: Coordinate = { row: '7', col: 'd' };

      const uncheckingMove = chessLogic.underAttackBoard.willKingBeUnderAttackAfterMove(moveFrom, moveTo);

      expect(uncheckingMove).toBeFalsy();
    });

    it('should update value that white king is under attack', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h1: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        b2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });

      expect(chessLogic.underAttackBoard.whiteKingUnderAttack.getValue()).toBeDefined();
      expect(chessLogic.underAttackBoard.whiteKingUnderAttack.getValue()).toBeTruthy();
    });

    it('should update value that black king is under attack', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        h1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        b2: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });

      expect(chessLogic.underAttackBoard.blackKingUnderAttack.getValue()).toBeDefined();
      expect(chessLogic.underAttackBoard.blackKingUnderAttack.getValue()).toBeTruthy();
    });

  });

});
