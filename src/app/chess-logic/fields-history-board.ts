import { BoardLayout } from './board-layout';
import { Board } from './models/board';
import { Coordinate } from './models/coordinate';
import { FieldHistory } from './models/field-history';

export type FieldsHistoryBoardSnapshot = Board<FieldHistory>;

export class FieldsHistoryBoard {
    private board: Board<FieldHistory>;

    constructor(snapshot?: FieldsHistoryBoardSnapshot) {
        if (snapshot) {
            this.board = snapshot as Board<FieldHistory>;
        } else {
            this.board = FieldsHistoryBoard.getNewFieldsHistoryBoard();
        }
    }

    public static createSnapshotFrom(history: {
        [encodedCoordinate: string]: FieldHistory
    }): FieldsHistoryBoardSnapshot {
        const board = FieldsHistoryBoard.getNewFieldsHistoryBoard();
        const encodedCoordinates = Object.keys(history);
        for (const encodedCoordinate of encodedCoordinates) {
            const coordinate = BoardLayout.decodeCoordinate(encodedCoordinate);
            board[coordinate.row][coordinate.col] = history[encodedCoordinate];
        }
        return board as FieldsHistoryBoardSnapshot;
    }

    public static getNewFieldsHistoryBoard(): Board<FieldHistory> {
        const board = {};
        for (const row of BoardLayout.rows) {
            board[row] = {};
            for (const col of BoardLayout.cols) {
                board[row][col] = {
                    attacked: false,
                    moved: false,
                    enPassantable: false
                };
            }
        }
        return board;
    }

    public getSnapshot(): FieldsHistoryBoardSnapshot {
        const board: FieldsHistoryBoardSnapshot = {};
        for (const row of BoardLayout.rows) {
            board[row] = {};
            for (const col of BoardLayout.cols) {
                board[row][col] = {
                    attacked: this.board[row][col].attacked,
                    moved: this.board[row][col].moved,
                    enPassantable: this.board[row][col].enPassantable
                };
            }
        }
        return board;
    }

    public get(coordinate: Coordinate): FieldHistory {
        if (BoardLayout.rows.includes(coordinate.row) &&
            BoardLayout.cols.includes(coordinate.col)) {
            return this.board[coordinate.row][coordinate.col];
        }
        return null;
    }

    public didFieldChangeSinceGameHadStarted(coordinate: Coordinate): boolean {
        const field = this.get(coordinate);
        return field.attacked === true || field.moved === true;
    }

    public registerMoved(coordinate: Coordinate): void {
        this.board[coordinate.row][coordinate.col].moved = true;
    }

    public registerAttacked(coordinate: Coordinate): void {
        this.board[coordinate.row][coordinate.col].attacked = true;
    }

    public registerEnPassantable(coordinate: Coordinate): void {
        this.board[coordinate.row][coordinate.col].enPassantable = true;
    }

    public resetAllEnPassant(): void {
        for (const row of BoardLayout.rows) {
            for (const col of BoardLayout.cols) {
                this.board[row][col].enPassantable = false;
            }
        }
    }
}
