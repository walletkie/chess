import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Col, Row } from 'src/app/chess-logic/board-layout';
import { ChessLogic } from 'src/app/chess-logic/chess-logic';
import { Coordinate } from 'src/app/chess-logic/models/coordinate';
import { Figure } from 'src/app/chess-logic/models/figure';
import { Move } from 'src/app/chess-logic/models/move';
import { MoveType } from 'src/app/chess-logic/models/move-type.enum';
import { PlayerService } from 'src/app/services/player.service';

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.scss']
})
export class FieldComponent implements OnInit, OnDestroy {

  public MoveType = MoveType;

  @Input()
  public chessLogic: ChessLogic;

  @Input()
  public row: Row;

  @Input()
  public col: Col;

  private selectedSubscription: Subscription;
  private figureSubscription: Subscription;
  private possibleMoveSubscription: Subscription;
  private lastMoveSubscription: Subscription;

  public isEvenField: boolean;
  public selected: boolean;
  public figure: Figure;
  public possibleMove: MoveType;
  public isLastMove: boolean;

  constructor(
    public playerService: PlayerService
  ) { }

  ngOnInit() {
    this.isEvenField = (this.row.charCodeAt(0) + this.col.charCodeAt(0)) % 2 === 0;

    this.selectedSubscription =
      this.chessLogic.selected
        .subscribe((selected) => this.onSelectedChange(selected));

    this.figureSubscription =
      this.chessLogic.figuresBoard.current[this.row][this.col]
        .subscribe((figure) => this.onFigureChange(figure));

    this.possibleMoveSubscription =
      this.chessLogic.possibleMovesBoard.current[this.row][this.col]
        .subscribe((possibleMove) => this.onPossibleMoveChange(possibleMove));

    this.lastMoveSubscription =
      this.chessLogic.lastMove
        .subscribe((lastMove) => this.onLastMoveChange(lastMove));
  }

  ngOnDestroy(): void {
    if (this.selectedSubscription) {
      this.selectedSubscription.unsubscribe();
    }
    if (this.figureSubscription) {
      this.figureSubscription.unsubscribe();
    }
    if (this.possibleMoveSubscription) {
      this.possibleMoveSubscription.unsubscribe();
    }
    if (this.lastMoveSubscription) {
      this.lastMoveSubscription.unsubscribe();
    }
  }

  private onLastMoveChange(lastMove: Move): void {
    const isLastMove = lastMove && (
      (lastMove.from.row === this.row && lastMove.from.col === this.col) || (lastMove.to.row === this.row && lastMove.to.col === this.col));
    if (this.isLastMove !== isLastMove) {
      this.isLastMove = isLastMove;
    }
  }

  private onSelectedChange(selected: Coordinate): void {
    const isSelected = !!selected && selected.row === this.row && selected.col === this.col;
    if (this.selected !== isSelected) {
      this.selected = isSelected;
    }
  }

  private onPossibleMoveChange(possibleMove: MoveType): void {
    if (this.possibleMove !== possibleMove) {
      this.possibleMove = possibleMove;
    }
  }

  private onFigureChange(figure: Figure): void {
    if (this.figure !== figure) {
      this.figure = figure;
    }
  }

}
