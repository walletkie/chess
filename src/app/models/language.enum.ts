export const DefaultLanguage = 'en';

export enum Language {
    'en' = 'English',
    'pl' = 'Polski'
}
