import { FieldsHistoryBoard } from './fields-history-board';
import { Coordinate } from './models/coordinate';

describe('FieldsHistoryBoard', () => {
  it('should create an instance', () => {
    expect(new FieldsHistoryBoard()).toBeTruthy();
  });

  it('should return field hisotry', () => {
    const fieldsHistoryBoard = new FieldsHistoryBoard();

    const hostory = fieldsHistoryBoard.get({ row: '8', col: 'h' });

    expect(hostory.attacked).toBe(false);
    expect(hostory.enPassantable).toBe(false);
    expect(hostory.moved).toBe(false);
  });

  it('should confirm that field has not changed since game started', () => {
    const fieldsHistoryBoard = new FieldsHistoryBoard();
    const field: Coordinate = { row: '2', col: 'a' };

    const fieldChanged = fieldsHistoryBoard.didFieldChangeSinceGameHadStarted(field);

    expect(fieldChanged).toBe(false);
  });

  it('should return snapshot', () => {
    const fieldsHistoryBoard = new FieldsHistoryBoard();
    const field: Coordinate = { row: '1', col: 'a' };
    fieldsHistoryBoard.registerMoved(field);

    const snapshot = fieldsHistoryBoard.getSnapshot();

    expect(snapshot[field.row][field.col].moved).toBe(true);
  });

  it('should restore from snapshot', () => {
    const fieldsHistoryBoard = new FieldsHistoryBoard();
    const field: Coordinate = { row: '1', col: 'a' };
    fieldsHistoryBoard.registerMoved(field);

    const snapshot = fieldsHistoryBoard.getSnapshot();
    const restoredFieldsHistoryBoard = new FieldsHistoryBoard(snapshot);
    const fieldChanged = restoredFieldsHistoryBoard.didFieldChangeSinceGameHadStarted(field);

    expect(fieldChanged).toBe(true);
  });

  it('should return snapshot from history array', () => {
    const snapshot = FieldsHistoryBoard.createSnapshotFrom({
      a1: { attacked: true, enPassantable: false, moved: false }
    });

    const fieldsHistoryBoard = new FieldsHistoryBoard(snapshot);
    const fieldWithModifiedHisotry: Coordinate = { row: '1', col: 'a' };
    const fieldWithNoHisotry: Coordinate = { row: '2', col: 'a' };

    expect(fieldsHistoryBoard.get(fieldWithModifiedHisotry).attacked).toBe(true);
    expect(fieldsHistoryBoard.get(fieldWithModifiedHisotry).enPassantable).toBe(false);
    expect(fieldsHistoryBoard.get(fieldWithModifiedHisotry).moved).toBe(false);
    expect(fieldsHistoryBoard.get(fieldWithNoHisotry).attacked).toBe(false);
    expect(fieldsHistoryBoard.get(fieldWithNoHisotry).enPassantable).toBe(false);
    expect(fieldsHistoryBoard.get(fieldWithNoHisotry).moved).toBe(false);
  });
});
