import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule, FirestoreSettingsToken } from '@angular/fire/firestore';
import { AngularFireFunctionsModule } from '@angular/fire/functions';
import { FormsModule } from '@angular/forms';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CookieService } from 'ngx-cookie-service';
import { environment } from 'src/environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChangeLanguageComponent } from './components/change-language/change-language.component';
import { BoardComponent } from './components/game/board/board.component';
import { FieldComponent } from './components/game/board/field/field.component';
import { FigureComponent } from './components/game/board/field/figure/figure.component';
import { ChoosePromotionFigureComponent } from './components/game/choose-promotion-figure/choose-promotion-figure.component';
import { ChooseSideComponent } from './components/game/choose-side/choose-side.component';
import { SidePanelComponent } from './components/game/choose-side/side-panel/side-panel.component';
import { GameComponent } from './components/game/game.component';
import { PlayerBannerComponent } from './components/game/player-banner/player-banner.component';
import { LoginComponent } from './components/login/login.component';
import { NavigationBarComponent } from './components/navigation-bar/navigation-bar.component';
import { PlayersListComponent } from './components/players-list/players-list.component';
import { RoomComponent } from './components/room/room.component';
import { SettingsComponent } from './components/settings/settings.component';
import { TestComponent } from './components/test/test.component';
import { AuthService } from './services/auth.service';


// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TestComponent,
    RoomComponent,
    GameComponent,
    PlayersListComponent,
    ChooseSideComponent,
    SidePanelComponent,
    BoardComponent,
    FieldComponent,
    ChoosePromotionFigureComponent,
    PlayerBannerComponent,
    FigureComponent,
    NavigationBarComponent,
    SettingsComponent,
    ChangeLanguageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    FontAwesomeModule,
    MatProgressSpinnerModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireFunctionsModule,
    MatButtonModule,
    MatTooltipModule,
    MatBadgeModule,
    MatInputModule,
    MatDialogModule,
    FormsModule,
    MatSelectModule,
    MatChipsModule,
    MatSnackBarModule,
    MatSlideToggleModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ],
  entryComponents: [
    ChoosePromotionFigureComponent,
    SettingsComponent,
  ],
  providers: [
    AuthService,
    CookieService,
    {
      provide: FirestoreSettingsToken,
      useValue: environment.production ? undefined : {
        host: 'localhost:8080',
        ssl: false
      }
    }
  ],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule { }
