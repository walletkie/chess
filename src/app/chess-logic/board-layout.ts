import { ChessLogic } from './chess-logic';
import { ChessFigure } from './models/chess-figure.enum';
import { Coordinate } from './models/coordinate';
import { EncodedMove } from './models/encoded-move';
import { Move } from './models/move';
import { MoveType } from './models/move-type.enum';
import { PlayerSide } from './models/player-side.enum';

export declare type Row = '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8';
export declare type Col = 'a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'h';

export class BoardLayout {

    public static readonly rows: Array<Row> = ['1', '2', '3', '4', '5', '6', '7', '8'];
    public static readonly cols: Array<Col> = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];

    public static get firstRow(): Row {
        return BoardLayout.rows[0];
    }

    public static get lastRow(): Row {
        return BoardLayout.rows[BoardLayout.rows.length - 1];
    }

    public static getRowPosition(row: Row, moveBy: number): Row {
        const index = BoardLayout.rows.indexOf(row);
        const newIndex = index + moveBy;
        if (newIndex >= 0 && newIndex < BoardLayout.rows.length) {
            return BoardLayout.rows[newIndex];
        }
        return null;
    }

    public static getColPosition(col: Col, moveBy: number): Col {
        const index = BoardLayout.cols.indexOf(col);
        const newIndex = index + moveBy;
        if (newIndex >= 0 && newIndex < BoardLayout.cols.length) {
            return BoardLayout.cols[newIndex];
        }
        return null;
    }

    public static getOponentSide(playerSide: PlayerSide): PlayerSide {
        if (playerSide === PlayerSide.WHITE) {
            return PlayerSide.BLACK;
        } else if (playerSide === PlayerSide.BLACK) {
            return PlayerSide.WHITE;
        }
        return null;
    }

    public static encodeMove(from: Coordinate, to: Coordinate, pormotionTo?: ChessFigure): EncodedMove {
        const fromPart = BoardLayout.encodeCoordinate(from);
        const toPart = BoardLayout.encodeCoordinate(to);
        if (pormotionTo) {
            return `${fromPart}-${toPart}:${pormotionTo}`;
        } else {
            return `${fromPart}-${toPart}`;
        }
    }

    public static encodeCoordinate(coordinate: Coordinate): string {
        return `${coordinate.col}${coordinate.row}`;
    }

    public static decodeCoordinate(encodedCoordinate: string): Coordinate {
        return {
            row: encodedCoordinate[1] as Row,
            col: encodedCoordinate[0] as Col
        };
    }

    public static decodeMove(chessLogic: ChessLogic, move: EncodedMove): Move {
        const splitedPromotionPart = move.split(':');
        const splitedMovePart = splitedPromotionPart[0].split('-');

        const fromCoordinate = BoardLayout.decodeCoordinate(splitedMovePart[0]);
        const toCoordinate = BoardLayout.decodeCoordinate(splitedMovePart[1]);
        const decodeMove = BoardLayout.decodeMoveType(
            chessLogic,
            fromCoordinate,
            toCoordinate
        );

        const decodedMove: Move = {
            from: fromCoordinate,
            to: toCoordinate,
            type: decodeMove
        };
        if (splitedPromotionPart[1]) {
            decodedMove.promotion = Number(splitedPromotionPart[1]);
        }

        return decodedMove;
    }

    private static decodeMoveType(chessLogic: ChessLogic, fromCoordinate: Coordinate, toCoordinate: Coordinate): MoveType {
        const selectedFigure = chessLogic.figuresBoard.get(fromCoordinate);

        switch (selectedFigure.chessFigure) {

            case ChessFigure.PAWN:
                return chessLogic.pawn.getMoveType(fromCoordinate, toCoordinate);

            case ChessFigure.BISHOP:
                return chessLogic.bishop.getMoveType(toCoordinate);

            case ChessFigure.KING:
                return chessLogic.king.getMoveType(fromCoordinate, toCoordinate);

            case ChessFigure.KNIGHT:
                return chessLogic.knight.getMoveType(toCoordinate);

            case ChessFigure.QUEEN:
                return chessLogic.queen.getMoveType(toCoordinate);

            case ChessFigure.ROCK:
                return chessLogic.rock.getMoveType(toCoordinate);

            default:
                throw new Error('Unhandled figure type in decoding move type');
        }

    }

}
