import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { faCog, faShareAlt } from '@fortawesome/free-solid-svg-icons';
import { TranslateService } from '@ngx-translate/core';
import { SettingsComponent } from '../settings/settings.component';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.scss']
})
export class NavigationBarComponent implements OnInit {

  public faCog = faCog;
  public faShareAlt = faShareAlt;

  shareRoomInput: string;

  @ViewChild('shareRoomInputRef', { static: true })
  shareRoomInputRef: ElementRef;

  constructor(
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private translateService: TranslateService,
  ) { }

  ngOnInit() {
    this.shareRoomInput = window.location.href;
  }

  async onShareRoom(): Promise<void> {
    const ref = this.shareRoomInputRef.nativeElement;
    ref.select();
    ref.setSelectionRange(0, 99999);
    document.execCommand('copy');
    this.snackBar.open(
      await this.translateService.get('navigation-bar.share-room.link-copied').toPromise(),
      await this.translateService.get('common.close').toPromise(),
      { duration: 1500 }
    );
  }

  onOptionsOpen(): void {
    this.dialog.open(SettingsComponent);
  }

}
