import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { faExclamationCircle, faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';
import { RoomService } from 'src/app/services/room.service';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit, OnDestroy {

  public faExclamationTriangle = faExclamationTriangle;
  public faExclamationCircle = faExclamationCircle;

  public roomIdSubscription: Subscription;

  constructor(
    public roomService: RoomService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.roomIdSubscription = this.route.paramMap.subscribe((paramMap) => {
      const roomId = paramMap.get('roomId');
      this.roomService.changeRoomId(roomId);
    });
  }


  ngOnDestroy(): void {
    if (this.roomIdSubscription) {
      this.roomIdSubscription.unsubscribe();
    }
    this.roomService.changeRoomId(undefined);
  }

}
