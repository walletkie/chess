import { Component, OnInit } from '@angular/core';
import { PlayerStatus } from 'src/app/models/player-status.enum';
import { PlayersListService } from 'src/app/services/players-list.service';

@Component({
  selector: 'app-players-list',
  templateUrl: './players-list.component.html',
  styleUrls: ['./players-list.component.scss']
})
export class PlayersListComponent implements OnInit {

  public PlayerStatus = PlayerStatus;

  constructor(
    public playersListService: PlayersListService
  ) { }

  ngOnInit() {
  }

}
