import { Injectable } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { Player } from '../models/player';
import { PlayerInRoom } from '../models/player-in-room';
import { PlayerStatus } from '../models/player-status.enum';
import { Room } from '../models/room';
import { PlayerService } from './player.service';
import { RoomService } from './room.service';

@Injectable({
  providedIn: 'root'
})
export class PlayersListService {

  public players: BehaviorSubject<{
    [uid: string]: PlayerInRoom
  }> = new BehaviorSubject({});
  private playersSubscriptions: {
    [uid: string]: Subscription
  } = {};

  constructor(
    private roomService: RoomService,
    private playerService: PlayerService
  ) {
    this.roomService.room.subscribe((roomData) => this.onRoomChange(roomData));
  }

  private onRoomChange(roomData: Room): void {
    if (!roomData) {
      return;
    }
    this.removeNotExistingPlayers(roomData.players);
    this.addNewPlayers(roomData.players);
  }

  private getFormatedPlayer(player: Player, playerStatus: PlayerStatus): PlayerInRoom {
    const avatar = player.avatarUrl || null;
    const name = player.displayedName || null;
    return {
      avatarUrl: avatar,
      displayedName: name,
      status: playerStatus
    };
  }

  private removeNotExistingPlayers(newPlayers: { [uid: string]: PlayerStatus }): void {
    for (const uid in this.players.getValue()) {
      if (uid in newPlayers === false) {
        this.removePlayer(uid);
      }
    }
  }

  private addNewPlayers(newPlayers: { [uid: string]: PlayerStatus }): void {
    for (const key in newPlayers) {
      if (newPlayers.hasOwnProperty(key)) {
        this.updateIfChanged(key, newPlayers[key]);
      }
    }
  }

  private removePlayer(uid: string): void {
    this.playersSubscriptions[uid].unsubscribe();
    const currentPlayers = this.players.getValue();
    delete currentPlayers[uid];
    delete this.playersSubscriptions[uid];
    this.players.next(currentPlayers);
  }

  private updateIfChanged(uid: string, playerStatus: PlayerStatus): void {
    const currentPlayers = this.players.getValue();

    if (uid in this.playersSubscriptions === false) {
      this.playersSubscriptions[uid] = this.playerService.getPlayer(uid).valueChanges().subscribe(
        (player) => {
          if (player) {
            currentPlayers[uid] = this.getFormatedPlayer(player as Player, playerStatus);
            this.players.next(currentPlayers);
          }
        }
      );
    }

    if (uid in currentPlayers) {
      if (currentPlayers[uid].status !== playerStatus) {
        currentPlayers[uid].status = playerStatus;
        this.players.next(currentPlayers);
      }
    }
  }
}
