export enum GameStatus {
    UNDEFINED,
    WHITE_WON,
    BLACK_WON,
    WHITE_SURRENDERED,
    BLACK_SURRENDERED,
    DRAW
}
