import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireFunctions } from '@angular/fire/functions';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  constructor(
    public router: Router,
    private firestore: AngularFirestore,
    public functions: AngularFireFunctions,
    private authService: AuthService,
  ) { }

  ngOnInit() {
  }

  test1() {
  }

  test2() {
  }
}
