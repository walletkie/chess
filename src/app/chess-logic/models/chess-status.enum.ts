export enum ChessStatus {
    AWAITS,
    WHITE_TURN,
    BLACK_TURN,
    WHITE_LOST,
    BLACK_LOST,
    DRAW
}
