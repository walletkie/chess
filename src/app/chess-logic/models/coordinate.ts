import { Col, Row } from '../board-layout';

export interface Coordinate {
    row: Row;
    col: Col;
}
