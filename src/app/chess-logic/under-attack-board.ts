import { BehaviorSubject } from 'rxjs';
import { BoardLayout } from './board-layout';
import { ChessLogic } from './chess-logic';
import { Board } from './models/board';
import { ChessFigure } from './models/chess-figure.enum';
import { Coordinate } from './models/coordinate';
import { PlayerSide } from './models/player-side.enum';

export class UnderAttackBoard {

    private whitesUnderAttackBoard: Board<boolean>;
    private blacksUnderAttackBoard: Board<boolean>;

    public whiteKingUnderAttack: BehaviorSubject<boolean>;
    public blackKingUnderAttack: BehaviorSubject<boolean>;

    constructor(
        private chessLogic: ChessLogic
    ) {
        this.whitesUnderAttackBoard = UnderAttackBoard.getNewUnderAttackBoard();
        this.blacksUnderAttackBoard = UnderAttackBoard.getNewUnderAttackBoard();
        if (!this.chessLogic.underAttackTestingInstance) {
            this.whiteKingUnderAttack = new BehaviorSubject(false);
            this.blackKingUnderAttack = new BehaviorSubject(false);
        }
    }

    private static getNewUnderAttackBoard(): Board<boolean> {
        const board: Board<boolean> = {};
        for (const row of BoardLayout.rows) {
            board[row] = {};
            for (const col of BoardLayout.cols) {
                board[row][col] = false;
            }
        }
        return board;
    }

    public get(coordinate: Coordinate, fromPerspectiveOf: PlayerSide): boolean {
        if (fromPerspectiveOf === PlayerSide.WHITE) {
            return this.whitesUnderAttackBoard[coordinate.row][coordinate.col];
        } else if (fromPerspectiveOf === PlayerSide.BLACK) {
            return this.blacksUnderAttackBoard[coordinate.row][coordinate.col];
        }
        return null;
    }

    private registerAttack(coordinate: Coordinate, fromPerspectiveOf: PlayerSide): void {
        if (fromPerspectiveOf === PlayerSide.WHITE) {
            this.blacksUnderAttackBoard[coordinate.row][coordinate.col] = true;
        } else if (fromPerspectiveOf === PlayerSide.BLACK) {
            this.whitesUnderAttackBoard[coordinate.row][coordinate.col] = true;
        }
    }

    private updateUderAttackFieldsFor(coordinate: Coordinate): void {
        const field = this.chessLogic.figuresBoard.get(coordinate);
        if (this.chessLogic.figuresBoard.isEmpty(coordinate) === false) {

            let underAttackFields: Array<Coordinate> = [];
            switch (field.chessFigure) {
                case ChessFigure.PAWN:
                    underAttackFields = this.chessLogic.pawn.getAllAttackableFields(coordinate);
                    break;

                case ChessFigure.BISHOP:
                    underAttackFields = this.chessLogic.bishop.getAllAttackableFields(coordinate);
                    break;

                case ChessFigure.KNIGHT:
                    underAttackFields = this.chessLogic.knight.getAllAttackableFields(coordinate);
                    break;

                case ChessFigure.QUEEN:
                    underAttackFields = this.chessLogic.queen.getAllAttackableFields(coordinate);
                    break;

                case ChessFigure.ROCK:
                    underAttackFields = this.chessLogic.rock.getAllAttackableFields(coordinate);
                    break;

                case ChessFigure.KING:
                    underAttackFields = this.chessLogic.king.getAllAttackableFields(coordinate);
                    break;

                default:
                    break;

            }

            for (const attack of underAttackFields) {
                this.registerAttack(attack, field.color);
            }
        }
    }

    public updateUnderAttackBoard(): void {
        for (const row of BoardLayout.rows) {
            for (const col of BoardLayout.cols) {
                this.whitesUnderAttackBoard[row][col] = false;
                this.blacksUnderAttackBoard[row][col] = false;
            }
        }
        for (const row of BoardLayout.rows) {
            for (const col of BoardLayout.cols) {
                this.updateUderAttackFieldsFor({ row, col });
            }
        }
        if (!this.chessLogic.underAttackTestingInstance) {
            const whiteKingPosition = this.chessLogic.figuresBoard.getKingPosition(PlayerSide.WHITE);
            const isWhiteKingUnderAttack = this.get(whiteKingPosition, PlayerSide.WHITE);
            if (this.whiteKingUnderAttack.getValue() !== isWhiteKingUnderAttack) {
                this.whiteKingUnderAttack.next(isWhiteKingUnderAttack);
            }
            const blackKingPosition = this.chessLogic.figuresBoard.getKingPosition(PlayerSide.BLACK);
            const isBlackKingUnderAttack = this.get(blackKingPosition, PlayerSide.BLACK);
            if (this.blackKingUnderAttack.getValue() !== isBlackKingUnderAttack) {
                this.blackKingUnderAttack.next(isBlackKingUnderAttack);
            }
        }
    }

    public willKingBeUnderAttackAfterMove(from: Coordinate, to: Coordinate): boolean {
        const figuresBoardSnapshot = this.chessLogic.figuresBoard.getSnapshot();
        const testingInstance = new ChessLogic({
            figuresBoardSnapshot,
            underAttackTestingInstance: true,
            playsAs: this.chessLogic.playsAs
        });
        testingInstance.updateMoves({
            1: BoardLayout.encodeMove(from, to)
        });
        testingInstance.underAttackBoard.updateUnderAttackBoard();
        const kingPosition = testingInstance.figuresBoard.getKingPosition(this.chessLogic.playsAs);
        return testingInstance.underAttackBoard.get(kingPosition, this.chessLogic.playsAs);
    }
}
