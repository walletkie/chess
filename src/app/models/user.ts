export interface User {
    isLoggedIn: boolean;
    uid?: string;
    isAnonymus?: boolean;

    email?: string;
    displayedName?: string;
    avatarUrl?: string;
}
