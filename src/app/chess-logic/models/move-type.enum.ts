export enum MoveType {
    NONE,
    MOVE,
    EN_PASSANT,
    ATTACK,
    PROMOTION,
    CASTLING
}
