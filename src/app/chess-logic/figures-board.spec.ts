import { FiguresBoard } from './figures-board';
import { ChessFigure } from './models/chess-figure.enum';
import { Coordinate } from './models/coordinate';
import { PlayerSide } from './models/player-side.enum';

describe('FiguresBoard', () => {
  it('should create an instance', () => {
    expect(new FiguresBoard()).toBeTruthy();
  });

  it('should return figure', () => {
    const figureBoard = new FiguresBoard();

    const figure = figureBoard.get({ row: '8', col: 'h' });

    expect(figure).toBeTruthy();
  });

  it('should be vaild default board', () => {
    const figureBoard = new FiguresBoard();

    // tested only a few random figures
    const blackKing = figureBoard.get({ row: '8', col: 'e' });
    const blackPawn = figureBoard.get({ row: '7', col: 'a' });
    const blackRock = figureBoard.get({ row: '8', col: 'h' });
    const whiteKing = figureBoard.get({ row: '1', col: 'e' });
    const whiteQueen = figureBoard.get({ row: '1', col: 'd' });
    const whiteBishop = figureBoard.get({ row: '1', col: 'c' });

    expect(blackKing.chessFigure).toBe(ChessFigure.KING);
    expect(blackKing.color).toBe(PlayerSide.BLACK);
    expect(blackPawn.chessFigure).toBe(ChessFigure.PAWN);
    expect(blackPawn.color).toBe(PlayerSide.BLACK);
    expect(blackRock.chessFigure).toBe(ChessFigure.ROCK);
    expect(blackRock.color).toBe(PlayerSide.BLACK);
    expect(whiteKing.chessFigure).toBe(ChessFigure.KING);
    expect(whiteKing.color).toBe(PlayerSide.WHITE);
    expect(whiteQueen.chessFigure).toBe(ChessFigure.QUEEN);
    expect(whiteQueen.color).toBe(PlayerSide.WHITE);
    expect(whiteBishop.chessFigure).toBe(ChessFigure.BISHOP);
    expect(whiteBishop.color).toBe(PlayerSide.WHITE);
  });

  it('should return snapshot', () => {
    const figureBoard = new FiguresBoard();

    const snapshot = figureBoard.getSnapshot();

    expect(snapshot).toBeTruthy();
  });

  it('should create instance from snapshot', () => {
    const figureBoard = new FiguresBoard();
    const testedField: Coordinate = { row: '2', col: 'a' };

    const snapshot = figureBoard.getSnapshot();
    snapshot[testedField.row][testedField.col].color = PlayerSide.BLACK;
    const newFiguresBoard = new FiguresBoard(snapshot);
    const figureInOldBoard = figureBoard.get(testedField);
    const figureInNewBoard = newFiguresBoard.get(testedField);

    expect(figureInOldBoard.chessFigure).toBe(ChessFigure.PAWN);
    expect(figureInOldBoard.color).toBe(PlayerSide.WHITE);
    expect(figureInNewBoard.chessFigure).toBe(ChessFigure.PAWN);
    expect(figureInNewBoard.color).toBe(PlayerSide.BLACK);
  });

  it('should create snapshot from figures array', () => {
    const snapshot = FiguresBoard.createSnapshotFrom({
      c7: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
      c6: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
      c1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
      c2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
    });


    const whiteKing: Coordinate = { row: '1', col: 'c' };
    const blackKing: Coordinate = { row: '7', col: 'c' };
    const whitePawn: Coordinate = { row: '2', col: 'c' };
    const blackPawn: Coordinate = { row: '6', col: 'c' };
    const emptyField: Coordinate = { row: '1', col: 'b' };
    expect(snapshot[whiteKing.row][whiteKing.col].chessFigure).toBe(ChessFigure.KING);
    expect(snapshot[whiteKing.row][whiteKing.col].color).toBe(PlayerSide.WHITE);
    expect(snapshot[blackKing.row][blackKing.col].chessFigure).toBe(ChessFigure.KING);
    expect(snapshot[blackKing.row][blackKing.col].color).toBe(PlayerSide.BLACK);
    expect(snapshot[whitePawn.row][whitePawn.col].chessFigure).toBe(ChessFigure.PAWN);
    expect(snapshot[whitePawn.row][whitePawn.col].color).toBe(PlayerSide.WHITE);
    expect(snapshot[blackPawn.row][blackPawn.col].chessFigure).toBe(ChessFigure.PAWN);
    expect(snapshot[blackPawn.row][blackPawn.col].color).toBe(PlayerSide.BLACK);
    expect(snapshot[emptyField.row][emptyField.col].chessFigure).toBe(ChessFigure.NONE);
    expect(snapshot[emptyField.row][emptyField.col].color).toBe(PlayerSide.NONE);
  });

  it('should confirm that field is empty', () => {
    const figureBoard = new FiguresBoard();
    const emptyField: Coordinate = { row: '3', col: 'a' };

    expect(figureBoard.isEmpty(emptyField)).toBeTruthy();
  });

  it('should confirm that field is not empty', () => {
    const figureBoard = new FiguresBoard();
    const emptyField: Coordinate = { row: '2', col: 'a' };

    expect(figureBoard.isEmpty(emptyField)).toBeFalsy();
  });

  it('should confirm that field is obtained by oponent', () => {
    const figureBoard = new FiguresBoard();
    const playerSide = PlayerSide.WHITE;
    const enemyField: Coordinate = { row: '7', col: 'a' };

    expect(figureBoard.isObtainedByOponent(playerSide, enemyField)).toBeTruthy();
  });

  it('should confirm that field is not obtained by oponent', () => {
    const figureBoard = new FiguresBoard();
    const playerSide = PlayerSide.WHITE;
    const enemyField: Coordinate = { row: '2', col: 'a' };

    expect(figureBoard.isObtainedByOponent(playerSide, enemyField)).toBeFalsy();
  });

  it('should confirm that field is obtained by allay', () => {
    const figureBoard = new FiguresBoard();
    const playerSide = PlayerSide.WHITE;
    const allayField: Coordinate = { row: '2', col: 'a' };

    expect(figureBoard.isObtainedByAllay(playerSide, allayField)).toBeTruthy();
  });

  it('should confirm that field is not obtained by allay', () => {
    const figureBoard = new FiguresBoard();
    const playerSide = PlayerSide.WHITE;
    const allayField: Coordinate = { row: '7', col: 'a' };

    expect(figureBoard.isObtainedByAllay(playerSide, allayField)).toBeFalsy();
  });
});
