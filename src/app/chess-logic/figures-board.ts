import { BehaviorSubject } from 'rxjs';
import { BoardLayout } from './board-layout';
import { Board } from './models/board';
import { ChessFigure } from './models/chess-figure.enum';
import { Coordinate } from './models/coordinate';
import { Figure } from './models/figure';
import { PlayerSide } from './models/player-side.enum';

export type FiguresBoardSnapshot = Board<Figure>;

export class FiguresBoard {

    public readonly current: Board<BehaviorSubject<Figure>>;
    private board: Board<Figure>;

    constructor(snapshot?: FiguresBoardSnapshot) {
        if (snapshot) {
            this.board = snapshot as Board<Figure>;
        } else {
            this.board = FiguresBoard.getDefaultBoard();
        }

        this.current = {};
        for (const row of BoardLayout.rows) {
            this.current[row] = {};
            for (const col of BoardLayout.cols) {
                this.current[row][col] = new BehaviorSubject({
                    chessFigure: ChessFigure.NONE,
                    color: PlayerSide.NONE
                });
            }
        }
        this.updateCurrentFiguresBoard();
    }

    public static getDefaultBoard(): Board<Figure> {
        const board = {};
        for (const row of BoardLayout.rows) {
            board[row] = {};
            for (const col of BoardLayout.cols) {
                board[row][col] = {
                    chessFigure: ChessFigure.NONE,
                    color: PlayerSide.NONE
                };
            }
        }

        // white figures
        for (const col of BoardLayout.cols) {
            board['2'][col].chessFigure = ChessFigure.PAWN;
        }
        board['1']['a'].chessFigure = ChessFigure.ROCK;
        board['1']['b'].chessFigure = ChessFigure.KNIGHT;
        board['1']['c'].chessFigure = ChessFigure.BISHOP;
        board['1']['d'].chessFigure = ChessFigure.QUEEN;
        board['1']['e'].chessFigure = ChessFigure.KING;
        board['1']['f'].chessFigure = ChessFigure.BISHOP;
        board['1']['g'].chessFigure = ChessFigure.KNIGHT;
        board['1']['h'].chessFigure = ChessFigure.ROCK;

        // white color
        for (const col of BoardLayout.cols) {
            board['1'][col].color = PlayerSide.WHITE;
            board['2'][col].color = PlayerSide.WHITE;
        }


        // black figures
        for (const col of BoardLayout.cols) {
            board['7'][col].chessFigure = ChessFigure.PAWN;
        }
        board['8']['a'].chessFigure = ChessFigure.ROCK;
        board['8']['b'].chessFigure = ChessFigure.KNIGHT;
        board['8']['c'].chessFigure = ChessFigure.BISHOP;
        board['8']['d'].chessFigure = ChessFigure.QUEEN;
        board['8']['e'].chessFigure = ChessFigure.KING;
        board['8']['f'].chessFigure = ChessFigure.BISHOP;
        board['8']['g'].chessFigure = ChessFigure.KNIGHT;
        board['8']['h'].chessFigure = ChessFigure.ROCK;

        // black color
        for (const col of BoardLayout.cols) {
            board['7'][col].color = PlayerSide.BLACK;
            board['8'][col].color = PlayerSide.BLACK;
        }

        return board;
    }

    public static createSnapshotFrom(figures: {
        [encodedCoordinate: string]: Figure
    }): FiguresBoardSnapshot {
        const board = {};
        for (const row of BoardLayout.rows) {
            board[row] = {};
            for (const col of BoardLayout.cols) {
                board[row][col] = {
                    chessFigure: ChessFigure.NONE,
                    color: PlayerSide.NONE
                };
            }
        }
        const encodedCoordinates = Object.keys(figures);
        for (const encodedCoordinate of encodedCoordinates) {
            const coordinate = BoardLayout.decodeCoordinate(encodedCoordinate);
            board[coordinate.row][coordinate.col] = figures[encodedCoordinate];
        }
        return board as FiguresBoardSnapshot;
    }

    public getSnapshot(): FiguresBoardSnapshot {
        const board: FiguresBoardSnapshot = {};
        for (const row of BoardLayout.rows) {
            board[row] = {};
            for (const col of BoardLayout.cols) {
                const figure = this.get({ row, col });
                board[row][col] = {
                    chessFigure: figure.chessFigure,
                    color: figure.color
                };
            }
        }
        return board;
    }

    public get(coordinate: Coordinate): Figure {
        if (BoardLayout.rows.includes(coordinate.row) &&
            BoardLayout.cols.includes(coordinate.col)) {
            return this.board[coordinate.row][coordinate.col];
        }
        return null;
    }

    public isEmpty(coordinate: Coordinate): boolean {
        const figure = this.get(coordinate);
        return figure.chessFigure === ChessFigure.NONE;
    }

    public isObtainedByAllay(player: PlayerSide, coordinate: Coordinate): boolean {
        const figure = this.get(coordinate);
        return figure.chessFigure !== ChessFigure.NONE && figure.color === player;
    }

    public isObtainedByOponent(player: PlayerSide, coordinate: Coordinate): boolean {
        const figure = this.get(coordinate);
        const oponentSide = BoardLayout.getOponentSide(player);
        return figure.chessFigure !== ChessFigure.NONE && figure.color === oponentSide;
    }

    public getKingPosition(ofColor: PlayerSide): Coordinate {
        for (const row of BoardLayout.rows) {
            for (const col of BoardLayout.cols) {
                const field = this.get({ row, col });
                if (field.chessFigure === ChessFigure.KING && field.color === ofColor) {
                    return { row, col };
                }
            }
        }
        throw new Error('Unable to locate king');
    }

    public takeFigure(coordinate: Coordinate): Figure {
        return this.putFigure(coordinate, {
            chessFigure: ChessFigure.NONE,
            color: PlayerSide.NONE
        });
    }

    public putFigure(coordinate: Coordinate, figure: Figure): Figure {
        const takenFigure = this.get(coordinate);
        const takenFigureCopy: Figure = {
            chessFigure: takenFigure.chessFigure,
            color: takenFigure.color
        };
        this.board[coordinate.row][coordinate.col] = figure;
        return takenFigureCopy;
    }

    public updateCurrentFiguresBoard(): void {
        for (const row of BoardLayout.rows) {
            for (const col of BoardLayout.cols) {
                const currentValue = this.current[row][col].getValue();
                const newValue = this.board[row][col];
                if (currentValue.chessFigure !== newValue.chessFigure ||
                    currentValue.color !== newValue.color) {
                    this.current[row][col].next({
                        chessFigure: newValue.chessFigure,
                        color: newValue.color
                    });
                }
            }
        }
    }

}
