import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { faChessBishop, faChessKnight, faChessQueen, faChessRook } from '@fortawesome/free-solid-svg-icons';
import { ChessFigure } from 'src/app/chess-logic/models/chess-figure.enum';

@Component({
  selector: 'app-choose-promotion-figure',
  templateUrl: './choose-promotion-figure.component.html',
  styleUrls: ['./choose-promotion-figure.component.scss']
})
export class ChoosePromotionFigureComponent implements OnInit {

  possibeFigures: Array<{
    chessFigure: ChessFigure,
    icon: any
  }>;

  selected: ChessFigure;

  constructor(
    private dialogRef: MatDialogRef<ChoosePromotionFigureComponent>
  ) { }

  ngOnInit() {
    this.possibeFigures = [
      {
        chessFigure: ChessFigure.BISHOP,
        icon: faChessBishop
      },
      {
        chessFigure: ChessFigure.KNIGHT,
        icon: faChessKnight
      },
      {
        chessFigure: ChessFigure.QUEEN,
        icon: faChessQueen
      },
      {
        chessFigure: ChessFigure.ROCK,
        icon: faChessRook
      }
    ];
  }

  onSelect(figure: ChessFigure): void {
    this.selected = figure;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onConfirm(): void {
    this.dialogRef.close(this.selected);
  }

}
