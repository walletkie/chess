import { BehaviorSubject } from 'rxjs';
import { BoardLayout } from './board-layout';
import { ChessLogic } from './chess-logic';
import { Board } from './models/board';
import { ChessFigure } from './models/chess-figure.enum';
import { Coordinate } from './models/coordinate';
import { Move } from './models/move';
import { MoveType } from './models/move-type.enum';

export class PossibleMovesBoard {

    public readonly current: Board<BehaviorSubject<MoveType>>;
    private board: Board<Board<MoveType>>;
    public isAvailableAnyMove: boolean;

    constructor(
        private chessLogic: ChessLogic
    ) {
        this.board = {};
        for (const row of BoardLayout.rows) {
            this.board[row] = {};
            for (const col of BoardLayout.cols) {
                this.board[row][col] = PossibleMovesBoard.getEmptyMoveTypeBoard();
            }
        }

        if (!this.chessLogic.underAttackTestingInstance) {
            this.current = {};
            for (const row of BoardLayout.rows) {
                this.current[row] = {};
                for (const col of BoardLayout.cols) {
                    this.current[row][col] = new BehaviorSubject(MoveType.NONE);
                }
            }
            chessLogic.selected.subscribe(() => this.updateCurrentPossibleMovesBoard());
        }
    }

    public static getEmptyMoveTypeBoard(): Board<MoveType> {
        const board = {};
        for (const row of BoardLayout.rows) {
            board[row] = {};
            for (const col of BoardLayout.cols) {
                board[row][col] = MoveType.NONE;
            }
        }
        return board;
    }

    public get(to: Coordinate, from: Coordinate = this.chessLogic.selected.getValue()): MoveType {
        return this.board[from.row][from.col][to.row][to.col];
    }

    private addAllMoves(moves: Array<Move>): void {
        for (const move of moves) {
            this.board[move.from.row][move.from.col][move.to.row][move.to.col] = move.type;
        }
    }

    private updateFigurePossibleMoves(coordinate: Coordinate): void {
        const figure = this.chessLogic.figuresBoard.get(coordinate);
        let possibleMoves: Array<Move>;

        switch (figure.chessFigure) {

            case ChessFigure.PAWN:
                possibleMoves = this.chessLogic.pawn.getAllPossibleMoves(coordinate);
                break;

            case ChessFigure.BISHOP:
                possibleMoves = this.chessLogic.bishop.getAllPossibleMoves(coordinate);
                break;

            case ChessFigure.KNIGHT:
                possibleMoves = this.chessLogic.knight.getAllPossibleMoves(coordinate);
                break;

            case ChessFigure.QUEEN:
                possibleMoves = this.chessLogic.queen.getAllPossibleMoves(coordinate);
                break;

            case ChessFigure.ROCK:
                possibleMoves = this.chessLogic.rock.getAllPossibleMoves(coordinate);
                break;

            case ChessFigure.KING:
                possibleMoves = this.chessLogic.king.getAllPossibleMoves(coordinate);
                break;

            default:
                throw new Error('Unhandled figure type in updating figure possible moves');
        }

        this.isAvailableAnyMove = this.isAvailableAnyMove || (possibleMoves.length > 0);
        this.addAllMoves(possibleMoves);
    }

    private updateCurrentPossibleMovesBoard(): void {
        const selectedField = this.chessLogic.selected.getValue();
        const board = (selectedField)
            ? this.board[selectedField.row][selectedField.col]
            : PossibleMovesBoard.getEmptyMoveTypeBoard();
        for (const row of BoardLayout.rows) {
            for (const col of BoardLayout.cols) {
                const currentValue = this.current[row][col].getValue();
                const newValue = board[row][col];
                if (currentValue !== newValue) {
                    this.current[row][col].next(newValue);
                }
            }
        }
    }

    public updatePossibleMoves(): void {
        for (const row of BoardLayout.rows) {
            for (const col of BoardLayout.cols) {
                this.board[row][col] = PossibleMovesBoard.getEmptyMoveTypeBoard();
            }
        }
        this.isAvailableAnyMove = false;

        for (const row of BoardLayout.rows) {
            for (const col of BoardLayout.cols) {
                const coordinate = { row, col };
                if (this.chessLogic.figuresBoard.isObtainedByAllay(this.chessLogic.playsAs, coordinate)) {
                    this.updateFigurePossibleMoves(coordinate);
                }
            }
        }
    }

}
