import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GameService } from 'src/app/services/game.service';
import { RoomService } from 'src/app/services/room.service';
import { AuthService } from 'src/app/services/auth.service';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { faCircle, faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { PlayersListService } from 'src/app/services/players-list.service';


@Component({
  selector: 'app-side-panel',
  templateUrl: './side-panel.component.html',
  styleUrls: ['./side-panel.component.scss']
})
export class SidePanelComponent implements OnInit {

  public faSignOutAlt = faSignOutAlt;
  public faCheckCircle = faCheckCircle;
  public faCircle = faCircle;

  @Input()
  public color: string;

  @Input()
  public playerUid: string;

  @Input()
  public playerReady: boolean;

  @Output()
  onJoin: EventEmitter<any> = new EventEmitter();

  @Output()
  onLeave: EventEmitter<any> = new EventEmitter();

  @Output()
  onReady: EventEmitter<any> = new EventEmitter();

  constructor(
    public roomService: RoomService,
    public authService: AuthService,
    public playersListService: PlayersListService
  ) { }

  ngOnInit() { }

}
