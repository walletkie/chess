import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faChess } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { PlayerService } from 'src/app/services/player.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  public faChess = faChess;

  private isReadySubscription: Subscription;
  private isLoggedInSubsription: Subscription;

  constructor(
    public authService: AuthService,
    public playerService: PlayerService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.initializeLoginRedirectHandler();
  }

  ngOnDestroy(): void {
    this.removeLoginRedirectHandler();
  }

  initializeLoginRedirectHandler(): void {
    this.isReadySubscription = this.authService.isReady.subscribe((isReady) => {
      if (isReady) {
        this.isLoggedInSubsription = this.authService.user.subscribe((user) => {
          if (user.isLoggedIn) {
            const returnUrl = this.route.snapshot.queryParams.returnUrl;
            if (!returnUrl) {
              this.router.navigate(['room', user.uid]);
            } else {
              this.router.navigateByUrl(returnUrl, { replaceUrl: true });
            }
          }
        });
      }
    });
  }

  removeLoginRedirectHandler(): void {
    if (this.isReadySubscription) {
      this.isReadySubscription.unsubscribe();
    }
    if (this.isLoggedInSubsription) {
      this.isLoggedInSubsription.unsubscribe();
    }
  }

}
