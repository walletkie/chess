import { Moves } from '../chess-logic/models/moves';
import { GameStatus } from './game-status.enum';

export interface Game {
    whitePlayerUid: string;
    whitePlayerReady: boolean;
    whitePlayerOffersADraw: boolean;
    blackPlayerUid: string;
    blackPlayerReady: boolean;
    blackPlayerOffersADraw: boolean;
    moves: Moves;
    result: GameStatus;
}
