import { EncodedMove } from './encoded-move';

export interface Moves {
    [id: number]: EncodedMove;
}
