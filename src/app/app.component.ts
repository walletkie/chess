import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { DefaultLanguage } from './models/language.enum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    private translateService: TranslateService,
    private titleService: Title,
    private translate: TranslateService,
    private cookieService: CookieService,
  ) { }

  ngOnInit(): void {
    const language = this.cookieService.get('language') || DefaultLanguage;
    this.cookieService.set('language', language);

    this.translate.setDefaultLang(DefaultLanguage);
    this.translate.use(language);

    this.translateService.onLangChange.subscribe(async () => {
      this.titleService.setTitle(
        await this.translateService.get('app.chess').toPromise()
      );
    });
  }

}
