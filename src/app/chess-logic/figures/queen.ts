import { BoardLayout } from '../board-layout';
import { ChessLogic } from '../chess-logic';
import { ChessFigure } from '../models/chess-figure.enum';
import { Coordinate } from '../models/coordinate';
import { Move } from '../models/move';
import { MoveType } from '../models/move-type.enum';
import { FigureBase } from './figure-base';

export class Queen extends FigureBase {

    private static readonly AVAILABLE_MOVES_DIRECTIONS: {
        [name: string]: { row: number, col: number }
    } = {
            UP: { row: 1, col: 0 },
            UP_RIGHT: { row: 1, col: 1 },
            RIGHT: { row: 0, col: 1 },
            DOWN_RIGHT: { row: -1, col: 1 },
            DOWN: { row: -1, col: 0 },
            DOWN_LEFT: { row: -1, col: -1 },
            LEFT: { row: 0, col: -1 },
            UP_LEFT: { row: 1, col: -1 },
        };

    constructor(
        private chessLogic: ChessLogic
    ) {
        super();
    }

    public getAllPossibleMoves(forField: Coordinate): Array<Move> {
        const possibleMoves: Array<Move> = [];

        const possibleToMoveFields = this.getPossibleToMoveFields(forField);
        for (const move of possibleToMoveFields) {
            possibleMoves.push({
                from: forField,
                to: move,
                type: MoveType.MOVE
            });
        }

        const possibleToAttackFields = this.getPossibleToAttackFields(forField);
        for (const attack of possibleToAttackFields) {
            possibleMoves.push({
                from: forField,
                to: attack,
                type: MoveType.ATTACK
            });
        }

        return possibleMoves;
    }

    public getPossibleToMoveFields(forField: Coordinate): Array<Coordinate> {
        const possibleToMoveFields: Array<Coordinate> = [];
        const allAttackableFields = this.getAllAttackableFields(forField);
        for (const move of allAttackableFields) {
            if (this.chessLogic.figuresBoard.isEmpty(move) &&
                !this.chessLogic.underAttackBoard.willKingBeUnderAttackAfterMove(forField, move)) {
                possibleToMoveFields.push(move);
            }
        }
        return possibleToMoveFields;
    }

    public getPossibleToAttackFields(forField: Coordinate): Array<Coordinate> {
        const possibleToAttackFields: Array<Coordinate> = [];
        const allAttackableFields = this.getAllAttackableFields(forField);
        for (const attack of allAttackableFields) {
            if (this.chessLogic.figuresBoard.isObtainedByOponent(this.chessLogic.playsAs, attack) &&
                !this.chessLogic.underAttackBoard.willKingBeUnderAttackAfterMove(forField, attack)) {
                possibleToAttackFields.push(attack);
            }
        }
        return possibleToAttackFields;
    }

    public getAllAttackableFields(forField: Coordinate): Array<Coordinate> {
        const allAttackableFields: Array<Coordinate> = [];
        for (const moveName in Queen.AVAILABLE_MOVES_DIRECTIONS) {
            const move = Queen.AVAILABLE_MOVES_DIRECTIONS[moveName];
            this.fillAttackableFieldsInDirection(forField, allAttackableFields, move.row, move.col);
        }
        return allAttackableFields;
    }

    private fillAttackableFieldsInDirection(
        field: Coordinate,
        attackableFields: Array<Coordinate>,
        rowDirection: number,
        colDirection: number
    ): void {
        for (let move = 1; move < BoardLayout.rows.length; move++) {
            const row = BoardLayout.getRowPosition(field.row, move * rowDirection);
            const col = BoardLayout.getColPosition(field.col, move * colDirection);
            if (row && col) {
                attackableFields.push({ row, col });
                if (this.chessLogic.figuresBoard.isEmpty({ row, col })) {
                    continue;
                } else {
                    break;
                }
            }
            break;
        }
    }

    public getMoveType(toCoordinate: Coordinate): MoveType {
        const destinationField = this.chessLogic.figuresBoard.get(toCoordinate);
        if (destinationField.chessFigure !== ChessFigure.NONE) {
            return MoveType.ATTACK;
        } else {
            return MoveType.MOVE;
        }
    }

}
