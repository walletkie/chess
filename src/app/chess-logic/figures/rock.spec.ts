import { ChessLogic } from '../chess-logic';
import { FiguresBoard } from '../figures-board';
import { ChessFigure } from '../models/chess-figure.enum';
import { Coordinate } from '../models/coordinate';
import { MoveType } from '../models/move-type.enum';
import { PlayerSide } from '../models/player-side.enum';
import { Rock } from './rock';

describe('Rock', () => {

  it('should create an instance', () => {
    const chessLogic = new ChessLogic({});
    expect(new Rock(chessLogic)).toBeTruthy();
  });

  describe('together with ChessLogic', () => {

    it('should return all attackable fields when some path are blocked', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d5: { chessFigure: ChessFigure.ROCK, color: PlayerSide.WHITE },
        d4: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const rock: Coordinate = { row: '5', col: 'd' };

      const allAttackableFields = chessLogic.rock.getAllAttackableFields(rock);

      expect(allAttackableFields).toBeDefined();
      expect(allAttackableFields.length).toBe(11);
      expect(allAttackableFields.some((e) => e.row === '5' && e.col === 'a')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '5' && e.col === 'b')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '5' && e.col === 'c')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '5' && e.col === 'e')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '5' && e.col === 'f')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '5' && e.col === 'g')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '5' && e.col === 'h')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '8' && e.col === 'd')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '7' && e.col === 'd')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '6' && e.col === 'd')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '4' && e.col === 'd')).toBeTruthy();
    });

    it('should return possible to attack fields when some path are blocked', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d5: { chessFigure: ChessFigure.ROCK, color: PlayerSide.WHITE },
        d4: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        d3: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
        d7: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const rock: Coordinate = { row: '5', col: 'd' };

      const possibleToAttackFields = chessLogic.rock.getPossibleToAttackFields(rock);

      expect(possibleToAttackFields).toBeDefined();
      expect(possibleToAttackFields.length).toBe(1);
      expect(possibleToAttackFields.some((e) => e.row === '7' && e.col === 'd')).toBeTruthy();
    });

    it('should return possible to attack fields when king is under attack', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        g4: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d5: { chessFigure: ChessFigure.ROCK, color: PlayerSide.WHITE },
        f5: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
        b5: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const rock: Coordinate = { row: '5', col: 'd' };

      const possibleToAttackFields = chessLogic.rock.getPossibleToAttackFields(rock);

      expect(possibleToAttackFields).toBeDefined();
      expect(possibleToAttackFields.length).toBe(1);
      expect(possibleToAttackFields.some((e) => e.row === '5' && e.col === 'f')).toBeTruthy();
    });

    it('should return possible to move fields when some path are blocked', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d5: { chessFigure: ChessFigure.ROCK, color: PlayerSide.WHITE },
        d6: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
        d4: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const rock: Coordinate = { row: '5', col: 'd' };

      const possibleToMoveFields = chessLogic.rock.getPossibleToMoveFields(rock);

      expect(possibleToMoveFields).toBeDefined();
      expect(possibleToMoveFields.length).toBe(7);
      expect(possibleToMoveFields.some((e) => e.row === '5' && e.col === 'a')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '5' && e.col === 'b')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '5' && e.col === 'c')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '5' && e.col === 'e')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '5' && e.col === 'f')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '5' && e.col === 'g')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '5' && e.col === 'h')).toBeTruthy();
    });

    it('should return possible to move fields to block attacker from checking king', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        f6: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d5: { chessFigure: ChessFigure.ROCK, color: PlayerSide.WHITE },
        f4: { chessFigure: ChessFigure.ROCK, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const rock: Coordinate = { row: '5', col: 'd' };

      const possibleToMoveFields = chessLogic.rock.getPossibleToMoveFields(rock);

      expect(possibleToMoveFields).toBeDefined();
      expect(possibleToMoveFields.length).toBe(1);
      expect(possibleToMoveFields.some((e) => e.row === '5' && e.col === 'f')).toBeTruthy();
    });

    it('should return possible to move fields keep king from beeing check', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        c5: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d5: { chessFigure: ChessFigure.ROCK, color: PlayerSide.WHITE },
        e5: { chessFigure: ChessFigure.ROCK, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const rock: Coordinate = { row: '5', col: 'd' };

      const possibleToMoveFields = chessLogic.rock.getPossibleToMoveFields(rock);

      expect(possibleToMoveFields).toBeDefined();
      expect(possibleToMoveFields.length).toBe(0);
    });

    it('should return all possible moves', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        b5: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d5: { chessFigure: ChessFigure.ROCK, color: PlayerSide.WHITE },
        f5: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const rock: Coordinate = { row: '5', col: 'd' };

      const allPossibleMoves = chessLogic.rock.getAllPossibleMoves(rock);

      expect(allPossibleMoves).toBeDefined();
      expect(allPossibleMoves.length).toBe(10);
      expect(allPossibleMoves.every((e) => e.from.row === '5' && e.from.col === 'd')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.type === MoveType.MOVE && e.to.row === '5' && e.to.col === 'c')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.type === MoveType.MOVE && e.to.row === '5' && e.to.col === 'e')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.type === MoveType.ATTACK && e.to.row === '5' && e.to.col === 'f')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.type === MoveType.MOVE && e.to.row === '8' && e.to.col === 'd')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.type === MoveType.MOVE && e.to.row === '7' && e.to.col === 'd')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.type === MoveType.MOVE && e.to.row === '6' && e.to.col === 'd')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.type === MoveType.MOVE && e.to.row === '4' && e.to.col === 'd')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.type === MoveType.MOVE && e.to.row === '3' && e.to.col === 'd')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.type === MoveType.MOVE && e.to.row === '2' && e.to.col === 'd')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.type === MoveType.MOVE && e.to.row === '1' && e.to.col === 'd')).toBeTruthy();
    });

    it('should return move move type', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d5: { chessFigure: ChessFigure.ROCK, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const destination: Coordinate = { row: '6', col: 'd' };

      const moveType = chessLogic.rock.getMoveType(destination);

      expect(moveType).toBeDefined();
      expect(moveType).toBe(MoveType.MOVE);
    });

    it('should return attack move type', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d5: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.WHITE },
        d6: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const destination: Coordinate = { row: '6', col: 'd' };

      const moveType = chessLogic.rock.getMoveType(destination);

      expect(moveType).toBeDefined();
      expect(moveType).toBe(MoveType.ATTACK);
    });

  });

});
