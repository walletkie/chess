import { BehaviorSubject } from 'rxjs';
import { BoardLayout } from './board-layout';
import { FieldsHistoryBoard, FieldsHistoryBoardSnapshot } from './fields-history-board';
import { FiguresBoard, FiguresBoardSnapshot } from './figures-board';
import { Bishop } from './figures/bishop';
import { King } from './figures/king';
import { Knight } from './figures/knight';
import { Pawn } from './figures/pawn';
import { Queen } from './figures/queen';
import { Rock } from './figures/rock';
import { ChessFigure } from './models/chess-figure.enum';
import { ChessStatus } from './models/chess-status.enum';
import { Coordinate } from './models/coordinate';
import { EncodedMove } from './models/encoded-move';
import { Figure } from './models/figure';
import { Move } from './models/move';
import { MoveType } from './models/move-type.enum';
import { Moves } from './models/moves';
import { PlayerSide } from './models/player-side.enum';
import { SelectErrorCode } from './models/select-error-code.enum';
import { PossibleMovesBoard } from './possible-moves-board';
import { UnderAttackBoard } from './under-attack-board';

export interface ChessLogicOptions {
    onMadeMove?: (moveId: number, encodedMove: EncodedMove) => void;

    underAttackTestingInstance?: boolean;
    figuresBoardSnapshot?: FiguresBoardSnapshot;
    fieldsHistoryBoardSnapshot?: FieldsHistoryBoardSnapshot;
    playsAs?: PlayerSide;
}

export class ChessLogic {

    public readonly pawn = new Pawn(this);
    public readonly rock = new Rock(this);
    public readonly king = new King(this);
    public readonly queen = new Queen(this);
    public readonly bishop = new Bishop(this);
    public readonly knight = new Knight(this);

    private onMadeMoveCallback: (moveId: number, encodedMove: EncodedMove) => void;

    public readonly movesHistory: Moves = {};
    private lastParsedId = 0;

    public readonly underAttackTestingInstance: boolean;

    public readonly figuresBoard: FiguresBoard;
    public readonly possibleMovesBoard: PossibleMovesBoard;
    public readonly fieldsHistoryBoard: FieldsHistoryBoard;
    public readonly underAttackBoard: UnderAttackBoard;
    public readonly playsAs: PlayerSide;
    public readonly selected = new BehaviorSubject<Coordinate>(null);
    public readonly status = new BehaviorSubject<ChessStatus>(ChessStatus.AWAITS);
    public readonly whiteBeatenFigures = new BehaviorSubject<Array<ChessFigure>>([]);
    public readonly blackBeatenFigures = new BehaviorSubject<Array<ChessFigure>>([]);
    public readonly lastMove = new BehaviorSubject<Move>(null);

    constructor(options: ChessLogicOptions) {
        this.underAttackTestingInstance = options.underAttackTestingInstance;
        this.playsAs = (options.playsAs in PlayerSide) ? options.playsAs : PlayerSide.WHITE;
        this.onMadeMoveCallback = (options.onMadeMove) ? options.onMadeMove : () => { };
        this.figuresBoard = new FiguresBoard(options.figuresBoardSnapshot);
        this.underAttackBoard = new UnderAttackBoard(this);
        this.fieldsHistoryBoard = new FieldsHistoryBoard(options.fieldsHistoryBoardSnapshot);
        if (!this.underAttackTestingInstance) {
            this.updateStatusOfPlayersTurn();
            this.underAttackBoard.updateUnderAttackBoard();
            this.possibleMovesBoard = new PossibleMovesBoard(this);
            this.possibleMovesBoard.updatePossibleMoves();
            this.updateStatusOfGameEnd();
        }
    }

    private isThisPlayerTurn(): boolean {
        return (this.status.getValue() === ChessStatus.WHITE_TURN && this.playsAs === PlayerSide.WHITE)
            || (this.status.getValue() === ChessStatus.BLACK_TURN && this.playsAs === PlayerSide.BLACK);
    }

    private updateStatusOfPlayersTurn(): void {
        // game always starts with white turn,
        // so % 2 of all moves determines which player`s turn is now
        if (Object.keys(this.movesHistory).length % 2 === 0) {
            this.status.next(ChessStatus.WHITE_TURN);
        } else {
            this.status.next(ChessStatus.BLACK_TURN);
        }
    }

    private updateStatusOfGameEnd(): void {
        if (this.isGameLost()) {
            if (this.playsAs === PlayerSide.WHITE) {
                this.status.next(ChessStatus.WHITE_LOST);
            } else if (this.playsAs === PlayerSide.BLACK) {
                this.status.next(ChessStatus.BLACK_LOST);
            } else {
                throw new Error('Unable to set game status, playsAs not meet required conditions');
            }
        } else if (this.isGameDraw()) {
            this.status.next(ChessStatus.DRAW);
        }
    }

    private updatePossibleMoves(): void {
        if (!this.underAttackTestingInstance && this.isThisPlayerTurn()) {
            this.possibleMovesBoard.updatePossibleMoves();
        }
    }

    private updateUnderAttackFields(): void {
        if (!this.underAttackTestingInstance) {
            this.underAttackBoard.updateUnderAttackBoard();
        }
    }

    public updateMoves(moves: Moves): void {
        const currentHistoryKeys = Object.keys(this.movesHistory);
        const newHistoryKeys = Object.keys(moves);

        for (let parsingId = currentHistoryKeys.length + 1; parsingId <= newHistoryKeys.length; parsingId++) {
            const move = moves[parsingId];
            this.parseMove(parsingId, move);
        }

        this.updateStatusOfPlayersTurn();
        this.updateUnderAttackFields();
        if (!this.underAttackTestingInstance) {
            this.updatePossibleMoves();
            this.figuresBoard.updateCurrentFiguresBoard();
            this.updateStatusOfGameEnd();
        }
    }

    private addBeatenFigure(figure: Figure): void {
        if (figure.color === PlayerSide.WHITE) {
            const currentBeatenList = this.whiteBeatenFigures.getValue();
            currentBeatenList.push(figure.chessFigure);
            this.whiteBeatenFigures.next(currentBeatenList);
        } else if (figure.color === PlayerSide.BLACK) {
            const currentBeatenList = this.blackBeatenFigures.getValue();
            currentBeatenList.push(figure.chessFigure);
            this.blackBeatenFigures.next(currentBeatenList);
        }
    }

    private parseMove(id: number, encodedMove: EncodedMove): void {
        this.movesHistory[id] = encodedMove;
        this.lastParsedId = id;
        const move = BoardLayout.decodeMove(this, encodedMove);

        this.fieldsHistoryBoard.resetAllEnPassant();
        switch (move.type) {
            case MoveType.MOVE: {
                const movedFigure = this.figuresBoard.takeFigure(move.from);
                this.figuresBoard.putFigure(move.to, movedFigure);
                this.fieldsHistoryBoard.registerMoved(move.from);
                this.fieldsHistoryBoard.registerMoved(move.to);
                if (Pawn.isTwoFieldsForwardMove(move)) {
                    this.fieldsHistoryBoard.registerEnPassantable({
                        row: move.to.row,
                        col: move.from.col
                    });
                }
                break;
            }

            case MoveType.ATTACK: {
                const attackingFigure = this.figuresBoard.takeFigure(move.from);
                const beatenFigure = this.figuresBoard.putFigure(move.to, attackingFigure);
                this.fieldsHistoryBoard.registerMoved(move.from);
                this.fieldsHistoryBoard.registerAttacked(move.to);
                this.addBeatenFigure(beatenFigure);
                break;
            }

            case MoveType.CASTLING: {
                const movedFigure = this.figuresBoard.takeFigure(move.from);
                this.fieldsHistoryBoard.registerMoved(move.from);
                this.figuresBoard.putFigure(move.to, movedFigure);
                this.fieldsHistoryBoard.registerMoved(move.to);
                const kingColor = movedFigure.color;
                const rockFrom: Coordinate = {
                    row: null,
                    col: null
                };
                const rockTo: Coordinate = {
                    row: null,
                    col: null
                };
                if (move.to.col === 'c') {
                    rockFrom.col = 'a';
                    rockTo.col = 'd';
                } else if (move.to.col === 'g') {
                    rockFrom.col = 'h';
                    rockTo.col = 'f';
                }
                if (kingColor === PlayerSide.WHITE) {
                    rockFrom.row = '1';
                    rockTo.row = '1';
                } else if (kingColor === PlayerSide.BLACK) {
                    rockFrom.row = '8';
                    rockTo.row = '8';
                }
                const rock = this.figuresBoard.takeFigure(rockFrom);
                this.figuresBoard.putFigure(rockTo, rock);
                this.fieldsHistoryBoard.registerMoved(rockFrom);
                this.fieldsHistoryBoard.registerMoved(rockTo);
                break;
            }

            case MoveType.EN_PASSANT: {
                const attackingFigure = this.figuresBoard.takeFigure(move.from);
                this.figuresBoard.putFigure(move.to, attackingFigure);
                const beatenFigure = this.figuresBoard.takeFigure({ row: move.from.row, col: move.to.col });
                this.fieldsHistoryBoard.registerMoved(move.from);
                this.fieldsHistoryBoard.registerAttacked(move.to);
                this.fieldsHistoryBoard.registerAttacked({ row: move.from.row, col: move.to.col });
                this.addBeatenFigure(beatenFigure);
                break;
            }

            case MoveType.PROMOTION: {
                const attackingFigure = this.figuresBoard.takeFigure(move.from);
                attackingFigure.chessFigure = move.promotion;
                const possiblyBeatenFigure = this.figuresBoard.putFigure(move.to, attackingFigure);
                this.fieldsHistoryBoard.registerMoved(move.from);
                if (possiblyBeatenFigure.chessFigure !== ChessFigure.NONE) {
                    this.fieldsHistoryBoard.registerAttacked(move.to);
                    this.addBeatenFigure(possiblyBeatenFigure);
                } else {
                    this.fieldsHistoryBoard.registerMoved(move.to);
                }
                break;
            }

            default: {
                throw new Error('Unhandled move type in move parsing');
            }
        }

        if (!this.underAttackTestingInstance) {
            this.lastMove.next(move);
        }
    }

    private makeMove(to: Coordinate, promotionTo?: ChessFigure): void {
        const encodedMove = BoardLayout.encodeMove(this.selected.getValue(), to, promotionTo);
        this.onMadeMoveCallback(this.lastParsedId + 1, encodedMove);
        this.status.next(ChessStatus.AWAITS);
    }

    public clickedAt(coordinate: Coordinate, promotionTo?: ChessFigure): void {
        if (!this.isThisPlayerTurn()) {
            throw SelectErrorCode.NOT_PLAYERS_TURN;
        }
        if (this.status.getValue() !== ChessStatus.WHITE_TURN && this.status.getValue() !== ChessStatus.BLACK_TURN) {
            throw SelectErrorCode.GAME_HAS_ENDED;
        }

        const selectedFigure = this.figuresBoard.get(coordinate);
        if (!this.selected.getValue()) {
            if (selectedFigure.chessFigure !== ChessFigure.NONE) {
                if (selectedFigure.color === this.playsAs) {
                    this.selected.next(coordinate);
                } else {
                    throw SelectErrorCode.SELECTED_OPONENTS_FIGURE;
                }
            }
        } else {
            if (coordinate.row === this.selected.getValue().row &&
                coordinate.col === this.selected.getValue().col) {
                this.selected.next(null);
            } else if (selectedFigure.chessFigure !== ChessFigure.NONE && selectedFigure.color === this.playsAs) {
                this.selected.next(coordinate);
            } else if (this.possibleMovesBoard.get(coordinate) !== MoveType.NONE) {
                if (this.possibleMovesBoard.get(coordinate) === MoveType.PROMOTION) {
                    if (!promotionTo) {
                        throw SelectErrorCode.PROMOTION_FIGURE_NOT_PROVIDED;
                    } else {
                        this.makeMove(coordinate, promotionTo);
                        this.selected.next(null);
                    }
                } else {
                    this.makeMove(coordinate);
                    this.selected.next(null);
                }
            }
        }
    }

    private isGameLost(): boolean {
        if (!this.isThisPlayerTurn()) {
            return false;
        }

        return this.possibleMovesBoard.isAvailableAnyMove === false
            && this.underAttackBoard.get(this.figuresBoard.getKingPosition(this.playsAs), this.playsAs);
    }

    private isGameDraw(): boolean {
        if (!this.isThisPlayerTurn()) {
            return false;
        }

        // TODO: game is drwa also if player makes the same moves 3 times
        return this.possibleMovesBoard.isAvailableAnyMove === false
            && !this.underAttackBoard.get(this.figuresBoard.getKingPosition(this.playsAs), this.playsAs);
    }
}
