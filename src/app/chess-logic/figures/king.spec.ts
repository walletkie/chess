import { ChessLogic } from '../chess-logic';
import { FiguresBoard } from '../figures-board';
import { ChessFigure } from '../models/chess-figure.enum';
import { Coordinate } from '../models/coordinate';
import { MoveType } from '../models/move-type.enum';
import { PlayerSide } from '../models/player-side.enum';
import { King } from './king';

describe('King', () => {
  it('should create an instance', () => {
    const chessLogic = new ChessLogic({});
    expect(new King(chessLogic)).toBeTruthy();
  });

  describe('together with ChessLogic', () => {

    it('should return possible to move fields', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        d4: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const king: Coordinate = { row: '4', col: 'd' };

      const possibleToMoveFields = chessLogic.king.getPossibleToMoveFields(king);

      expect(possibleToMoveFields).toBeDefined();
      expect(possibleToMoveFields.length).toBe(8);
      expect(possibleToMoveFields.some((e) => e.row === '5' && e.col === 'c')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '5' && e.col === 'd')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '5' && e.col === 'e')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '3' && e.col === 'c')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '3' && e.col === 'd')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '3' && e.col === 'e')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '4' && e.col === 'c')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '4' && e.col === 'e')).toBeTruthy();
    });

    it('should return possible to move fields when dome fields are under attack and some are obteined by allay', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        f5: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d6: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
        f6: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const king: Coordinate = { row: '5', col: 'f' };

      const possibleToMoveFields = chessLogic.king.getPossibleToMoveFields(king);

      expect(possibleToMoveFields).toBeDefined();
      expect(possibleToMoveFields.length).toBe(6);
      expect(possibleToMoveFields.some((e) => e.row === '6' && e.col === 'e')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '6' && e.col === 'g')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '4' && e.col === 'e')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '4' && e.col === 'f')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '4' && e.col === 'e')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '5' && e.col === 'g')).toBeTruthy();
    });

    it('should return possible to castling fields', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        c3: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        a1: { chessFigure: ChessFigure.ROCK, color: PlayerSide.WHITE },
        h1: { chessFigure: ChessFigure.ROCK, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const king: Coordinate = { row: '1', col: 'e' };

      const possibleToCastlingFields = chessLogic.king.getPossibleToCastlingFields(king);

      expect(possibleToCastlingFields).toBeDefined();
      expect(possibleToCastlingFields.length).toBe(2);
      expect(possibleToCastlingFields.some((e) => e.row === '1' && e.col === 'c')).toBeTruthy();
      expect(possibleToCastlingFields.some((e) => e.row === '1' && e.col === 'g')).toBeTruthy();
    });

    it('should return possible to castling fields when some fields are under attack', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        a1: { chessFigure: ChessFigure.ROCK, color: PlayerSide.WHITE },
        h1: { chessFigure: ChessFigure.ROCK, color: PlayerSide.WHITE },
        g2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
        a2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const king: Coordinate = { row: '1', col: 'e' };

      const possibleToCastlingFields = chessLogic.king.getPossibleToCastlingFields(king);

      expect(possibleToCastlingFields).toBeDefined();
      expect(possibleToCastlingFields.length).toBe(1);
      expect(possibleToCastlingFields.some((e) => e.row === '1' && e.col === 'c')).toBeTruthy();
    });

    it('should return all attackable fields', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        d4: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const king: Coordinate = { row: '4', col: 'd' };

      const allAttackableFields = chessLogic.king.getAllAttackableFields(king);

      expect(allAttackableFields).toBeDefined();
      expect(allAttackableFields.length).toBe(8);
      expect(allAttackableFields.some((e) => e.row === '5' && e.col === 'c')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '5' && e.col === 'd')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '5' && e.col === 'e')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '3' && e.col === 'c')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '3' && e.col === 'd')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '3' && e.col === 'e')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '4' && e.col === 'c')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '4' && e.col === 'e')).toBeTruthy();
    });

    it('should return possible to attack fields when some fields are under attack', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        f5: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        d3: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        e4: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        g5: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.BLACK
      });
      const king: Coordinate = { row: '5', col: 'f' };

      const attackableFields = chessLogic.king.getPossibleToAttackFields(king);

      expect(attackableFields).toBeDefined();
      expect(attackableFields.length).toBe(1);
      expect(attackableFields.some((e) => e.row === '5' && e.col === 'g')).toBeTruthy();
    });

    it('should return all possible to move fields when some are under attack and some are blocked', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        f5: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        e5: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
        d3: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        e4: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        g5: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.BLACK
      });
      const king: Coordinate = { row: '5', col: 'f' };

      const allPossibleMoves = chessLogic.king.getAllPossibleMoves(king);

      expect(allPossibleMoves).toBeDefined();
      expect(allPossibleMoves.length).toBe(5);
      expect(allPossibleMoves.every((e) => e.from.row === '5' && e.from.col === 'f')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.to.row === '6' && e.to.col === 'e')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.to.row === '6' && e.to.col === 'g')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.to.row === '5' && e.to.col === 'g')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.to.row === '4' && e.to.col === 'f')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.to.row === '4' && e.to.col === 'g')).toBeTruthy();
    });

    it('should return move move type', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        f5: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const king: Coordinate = { row: '5', col: 'f' };
      const destination: Coordinate = { row: '6', col: 'f' };

      const moveType = chessLogic.king.getMoveType(king, destination);

      expect(moveType).toBeDefined();
      expect(moveType).toBe(MoveType.MOVE);
    });

    it('should return attack move type', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        f5: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        f6: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const king: Coordinate = { row: '5', col: 'f' };
      const destination: Coordinate = { row: '6', col: 'f' };

      const moveType = chessLogic.king.getMoveType(king, destination);

      expect(moveType).toBeDefined();
      expect(moveType).toBe(MoveType.ATTACK);
    });

    it('should return castling move type', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        e1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        h1: { chessFigure: ChessFigure.ROCK, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const king: Coordinate = { row: '1', col: 'e' };
      const destination: Coordinate = { row: '1', col: 'g' };

      const moveType = chessLogic.king.getMoveType(king, destination);

      expect(moveType).toBeDefined();
      expect(moveType).toBe(MoveType.CASTLING);
    });

  });
});
