// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAz85sbMpSXkRmgz26GBWuvo9Ys6PTbtCI',
    authDomain: 'chess-50a20.firebaseapp.com',
    databaseURL: 'https://chess-50a20.firebaseio.com',
    projectId: 'chess-50a20',
    storageBucket: 'chess-50a20.appspot.com',
    messagingSenderId: '196890898456',
    appId: '1:196890898456:web:3b56f48058260a0729936d',
    measurementId: 'G-MYR97YTE4Z'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
