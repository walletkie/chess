import { ChessLogic } from '../chess-logic';
import { FiguresBoard } from '../figures-board';
import { ChessFigure } from '../models/chess-figure.enum';
import { Coordinate } from '../models/coordinate';
import { MoveType } from '../models/move-type.enum';
import { PlayerSide } from '../models/player-side.enum';
import { Queen } from './queen';

describe('Queen', () => {

  it('should create an instance', () => {
    const chessLogic = new ChessLogic({});
    expect(new Queen(chessLogic)).toBeTruthy();
  });

  describe('together with ChessLogic', () => {

    it('should return all attackable fields when some path are blocked', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d4: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.WHITE },
        b2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        b4: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        b6: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        f2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        f4: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        f6: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        d2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        d6: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const queen: Coordinate = { row: '4', col: 'd' };

      const allAttackableFields = chessLogic.queen.getAllAttackableFields(queen);

      expect(allAttackableFields).toBeDefined();
      expect(allAttackableFields.length).toBe(16);
      expect(allAttackableFields.some((e) => e.row === '2' && e.col === 'b')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '4' && e.col === 'b')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '6' && e.col === 'b')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '3' && e.col === 'c')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '4' && e.col === 'c')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '5' && e.col === 'c')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '2' && e.col === 'd')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '2' && e.col === 'd')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '5' && e.col === 'd')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '6' && e.col === 'd')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '3' && e.col === 'e')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '4' && e.col === 'e')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '5' && e.col === 'e')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '2' && e.col === 'f')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '4' && e.col === 'f')).toBeTruthy();
      expect(allAttackableFields.some((e) => e.row === '6' && e.col === 'f')).toBeTruthy();
    });

    it('should return possible to attack fields when some path are blocked', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d4: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.WHITE },
        e4: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        c3: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
        f4: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const queen: Coordinate = { row: '4', col: 'd' };

      const possibleToAttackFields = chessLogic.queen.getPossibleToAttackFields(queen);

      expect(possibleToAttackFields).toBeDefined();
      expect(possibleToAttackFields.length).toBe(1);
      expect(possibleToAttackFields.some((e) => e.row === '3' && e.col === 'c')).toBeTruthy();
    });

    it('should return possible to attack fields when king is under attack', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a4: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d4: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.WHITE },
        a7: { chessFigure: ChessFigure.ROCK, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const queen: Coordinate = { row: '4', col: 'd' };

      const possibleToAttackFields = chessLogic.queen.getPossibleToAttackFields(queen);

      expect(possibleToAttackFields).toBeDefined();
      expect(possibleToAttackFields.length).toBe(1);
      expect(possibleToAttackFields.some((e) => e.row === '7' && e.col === 'a')).toBeTruthy();
    });

    it('should return possible to move fields when some path are blocked', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d4: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.WHITE },
        b2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        b4: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        b6: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        f2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.WHITE },
        f4: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
        f6: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
        d2: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
        d6: { chessFigure: ChessFigure.PAWN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const queen: Coordinate = { row: '4', col: 'd' };

      const possibleToMoveFields = chessLogic.queen.getPossibleToMoveFields(queen);

      expect(possibleToMoveFields).toBeDefined();
      expect(possibleToMoveFields.length).toBe(8);
      expect(possibleToMoveFields.some((e) => e.row === '3' && e.col === 'c')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '4' && e.col === 'c')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '5' && e.col === 'c')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '3' && e.col === 'd')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '5' && e.col === 'd')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '3' && e.col === 'e')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '4' && e.col === 'e')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '5' && e.col === 'e')).toBeTruthy();
    });

    it('should return possible to move fields to block attacker from checking king', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d4: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.WHITE },
        a7: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const queen: Coordinate = { row: '4', col: 'd' };

      const possibleToMoveFields = chessLogic.queen.getPossibleToMoveFields(queen);

      expect(possibleToMoveFields).toBeDefined();
      expect(possibleToMoveFields.length).toBe(1);
      expect(possibleToMoveFields.some((e) => e.row === '4' && e.col === 'a')).toBeTruthy();
    });

    it('should return possible to move fields keep king from beeing check', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a4: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        d4: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.BLACK },
        e4: { chessFigure: ChessFigure.ROCK, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.BLACK
      });
      const queen: Coordinate = { row: '4', col: 'd' };

      const possibleToMoveFields = chessLogic.queen.getPossibleToMoveFields(queen);

      expect(possibleToMoveFields).toBeDefined();
      expect(possibleToMoveFields.length).toBe(2);
      expect(possibleToMoveFields.some((e) => e.row === '4' && e.col === 'b')).toBeTruthy();
      expect(possibleToMoveFields.some((e) => e.row === '4' && e.col === 'c')).toBeTruthy();

    });

    it('should return all possible moves which means blocking enemy queen or attacking it', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d4: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.WHITE },
        a7: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const queen: Coordinate = { row: '4', col: 'd' };

      const allPossibleMoves = chessLogic.queen.getAllPossibleMoves(queen);

      expect(allPossibleMoves).toBeDefined();
      expect(allPossibleMoves.length).toBe(2);
      expect(allPossibleMoves.every((e) => e.from.row === '4' && e.from.col === 'd')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.type === MoveType.MOVE && e.to.row === '4' && e.to.col === 'a')).toBeTruthy();
      expect(allPossibleMoves.some((e) => e.type === MoveType.ATTACK && e.to.row === '7' && e.to.col === 'a')).toBeTruthy();
    });

    it('should return move move type', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d4: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.WHITE },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const destination: Coordinate = { row: '5', col: 'd' };

      const moveType = chessLogic.queen.getMoveType(destination);

      expect(moveType).toBeDefined();
      expect(moveType).toBe(MoveType.MOVE);
    });

    it('should return attack move type', () => {
      const figuresBoardSnapshot = FiguresBoard.createSnapshotFrom({
        a1: { chessFigure: ChessFigure.KING, color: PlayerSide.WHITE },
        h2: { chessFigure: ChessFigure.KING, color: PlayerSide.BLACK },
        d4: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.WHITE },
        d5: { chessFigure: ChessFigure.QUEEN, color: PlayerSide.BLACK },
      });
      const chessLogic = new ChessLogic({
        figuresBoardSnapshot,
        playsAs: PlayerSide.WHITE
      });
      const destination: Coordinate = { row: '5', col: 'd' };

      const moveType = chessLogic.queen.getMoveType(destination);

      expect(moveType).toBeDefined();
      expect(moveType).toBe(MoveType.ATTACK);
    });

  });

});
