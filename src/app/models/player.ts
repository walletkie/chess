export interface Player {
    displayedName?: string;
    avatarUrl?: string;
}
