import { Component, OnInit } from '@angular/core';
import { faChess } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from 'src/app/services/auth.service';
import { GameService } from 'src/app/services/game.service';
import { RoomService } from 'src/app/services/room.service';

@Component({
  selector: 'app-choose-side',
  templateUrl: './choose-side.component.html',
  styleUrls: ['./choose-side.component.scss']
})
export class ChooseSideComponent implements OnInit {

  public faChess = faChess;

  constructor(
    public gameService: GameService,
    public authService: AuthService,
    public roomService: RoomService
  ) { }

  ngOnInit(): void {
  }

  onJoinWhite(): void {
    const game = this.gameService.game.getValue();
    const user = this.authService.user.getValue();
    if (game.blackPlayerUid === user.uid) {
      this.gameService.update({
        whitePlayerUid: user.uid,
        blackPlayerUid: null,
        blackPlayerReady: false
      });
    } else {
      this.gameService.update({
        whitePlayerUid: user.uid
      });
    }
  }

  onJoinBlack(): void {
    const game = this.gameService.game.getValue();
    const user = this.authService.user.getValue();
    if (game.whitePlayerUid === user.uid) {
      this.gameService.update({
        whitePlayerUid: null,
        blackPlayerUid: user.uid,
        whitePlayerReady: false
      });
    } else {
      this.gameService.update({
        blackPlayerUid: user.uid
      });
    }
  }

  onLeaveWhite(): void {
    this.gameService.update({
      whitePlayerUid: null,
      whitePlayerReady: false
    });
  }

  onLeaveBlack(): void {
    this.gameService.update({
      blackPlayerUid: null,
      blackPlayerReady: false
    });
  }

  onReadyWhite(): void {
    this.gameService.update({
      whitePlayerReady: true
    });
  }

  onReadyBlack(): void {
    this.gameService.update({
      blackPlayerReady: true
    });
  }

  onNotReadyWhite(): void {
    this.gameService.update({
      whitePlayerReady: false
    });
  }

  onNotReadyBlack(): void {
    this.gameService.update({
      blackPlayerReady: false
    });
  }

}
