import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { BehaviorSubject, Subscription } from 'rxjs';
import { PlayerSide } from '../chess-logic/models/player-side.enum';
import { Game } from '../models/game';
import { GameStatus } from '../models/game-status.enum';
import { Room } from '../models/room';
import { AuthService } from './auth.service';
import { RoomService } from './room.service';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  public readonly gameId = new BehaviorSubject<string>(undefined);
  public readonly game = new BehaviorSubject<Game>(undefined);

  private gameDocument: AngularFirestoreDocument<Game>;
  private gameDocumentSubscription: Subscription;

  constructor(
    private firestore: AngularFirestore,
    private authService: AuthService,
    private roomService: RoomService
  ) {
    this.roomService.room.subscribe((roomData) => this.onRoomChange(roomData));
  }

  private onRoomChange(roomData: Room): void {
    if (!roomData && this.gameId.getValue() !== undefined) {
      this.onGameIdChange(undefined);
    }
    if (roomData && roomData.game !== this.gameId.getValue()) {
      this.onGameIdChange(roomData.game);
    }
  }

  private onGameIdChange(gameId: string): void {
    if (this.gameDocumentSubscription) {
      this.gameDocumentSubscription.unsubscribe();
    }

    this.gameId.next(gameId);
    this.game.next(undefined);

    if (gameId) {
      this.gameDocument = this.firestore.collection('games').doc<Game>(gameId);
      this.gameDocumentSubscription = this.gameDocument.valueChanges().subscribe((gameData) => this.onGameChange(gameData));
    } else if (gameId === null && this.roomService.isRoomOwner.getValue()) {
      this.onCreateGame();
    }
  }

  private onGameChange(gameData: Game): void {
    if (!gameData) {
      this.game.next(null);
    } else {
      this.game.next(gameData);
    }
  }

  public async onCreateGame(): Promise<void> {
    const game: Game = {
      whitePlayerUid: this.authService.user.getValue().uid,
      whitePlayerReady: false,
      whitePlayerOffersADraw: false,

      blackPlayerUid: null,
      blackPlayerReady: false,
      blackPlayerOffersADraw: false,

      moves: {},
      result: GameStatus.UNDEFINED
    };
    const newGameDocument = await this.firestore.collection('games').add(game);
    await this.roomService.updateGameIdInRoom(newGameDocument.id);
  }

  public async update(gamePartial: Partial<Game>): Promise<void> {
    await this.gameDocument.update(gamePartial);
  }

  public playsAs(): PlayerSide {
    const uid = this.authService.user.getValue().uid;
    const game = this.game.getValue();
    if (game.whitePlayerUid === uid) {
      return PlayerSide.WHITE;
    } else if (game.blackPlayerUid === uid) {
      return PlayerSide.BLACK;
    } else {
      return PlayerSide.NONE;
    }
  }

  public playsInThisGame(): boolean {
    return this.playsAs() !== PlayerSide.NONE;
  }

  public onOfferDraw(offering: boolean): void {
    const playsAs = this.playsAs();
    if (playsAs === PlayerSide.WHITE) {
      if (this.game.getValue().blackPlayerOffersADraw && offering) {
        this.update({ result: GameStatus.DRAW });
      } else {
        this.update({ whitePlayerOffersADraw: offering });
      }
    } else if (playsAs === PlayerSide.BLACK) {
      if (this.game.getValue().whitePlayerOffersADraw && offering) {
        this.update({ result: GameStatus.DRAW });
      } else {
        this.update({ blackPlayerOffersADraw: offering });
      }
    }
  }

  public onAcceptDraw(): void {
    this.update({ result: GameStatus.DRAW });
  }

  public onSurrender(): void {
    const playsAs = this.playsAs();
    if (playsAs === PlayerSide.WHITE) {
      this.update({ result: GameStatus.WHITE_SURRENDERED });
    } else if (playsAs === PlayerSide.BLACK) {
      this.update({ result: GameStatus.BLACK_SURRENDERED });
    }
  }

}
