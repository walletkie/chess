import { Component, OnDestroy, OnInit } from '@angular/core';
import { faExclamationCircle, faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';
import { BoardLayout } from 'src/app/chess-logic/board-layout';
import { ChessLogic } from 'src/app/chess-logic/chess-logic';
import { ChessStatus } from 'src/app/chess-logic/models/chess-status.enum';
import { EncodedMove } from 'src/app/chess-logic/models/encoded-move';
import { PlayerSide } from 'src/app/chess-logic/models/player-side.enum';
import { Game } from 'src/app/models/game';
import { GameStatus } from 'src/app/models/game-status.enum';
import { GameService } from 'src/app/services/game.service';
import { PlayerService } from 'src/app/services/player.service';
import { RoomService } from 'src/app/services/room.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit, OnDestroy {

  public faExclamationTriangle = faExclamationTriangle;
  public faExclamationCircle = faExclamationCircle;
  public GameStatus = GameStatus;

  private chessStatusSubscription: Subscription;
  private gameSubscription: Subscription;

  public chessLogic: ChessLogic;

  private turnAudio = new Audio();

  constructor(
    public gameService: GameService,
    public roomService: RoomService,
    public playerService: PlayerService
  ) { }

  ngOnInit(): void {
    this.initializeTurnAudio();
    this.gameSubscription = this.gameService.game.subscribe((gameData) => this.onGameChanges(gameData));
  }

  ngOnDestroy(): void {
    if (this.gameSubscription) {
      this.gameSubscription.unsubscribe();
    }
    if (this.chessStatusSubscription) {
      this.chessStatusSubscription.unsubscribe();
    }
  }

  private initializeTurnAudio(): void {
    this.turnAudio.src = './assets/tone-beep.wav';
    this.turnAudio.autoplay = false;
    this.turnAudio.load();
  }

  private playTurnAudio(): void {
    if (this.playerService.settings.getValue().sound) {
      this.turnAudio.currentTime = 0;
      try {
        this.turnAudio.play();
      } catch (error) { }
    }
  }

  private playVibrations(): void {
    if (this.playerService.settings.getValue().vibrations) {
      navigator.vibrate(200);
    }
  }

  private onMadeMove(moveId: number, encodedMove: EncodedMove): void {
    this.gameService.update({
      [`moves.${moveId}`]: encodedMove
    });
  }

  private onChessStatusChanged(chessStatus: ChessStatus): void {
    const playsAs = this.gameService.playsAs();
    if (chessStatus === ChessStatus.BLACK_LOST && playsAs === PlayerSide.BLACK) {
      this.gameService.update({
        [`result`]: GameStatus.WHITE_WON
      });
    } else if (chessStatus === ChessStatus.WHITE_LOST && playsAs === PlayerSide.WHITE) {
      this.gameService.update({
        [`result`]: GameStatus.BLACK_WON
      });
    } else if (chessStatus === ChessStatus.DRAW) {
      this.gameService.update({
        [`result`]: GameStatus.DRAW
      });
    }
  }

  private onGameChanges(gameData: Game): void {
    if (!gameData || (!gameData.whitePlayerReady || !gameData.blackPlayerReady)) {
      if (this.chessStatusSubscription) {
        this.chessStatusSubscription.unsubscribe();
      }
      this.chessLogic = null;
    }
    if (gameData && gameData.whitePlayerReady && gameData.blackPlayerReady) {
      if (!this.chessLogic) {
        this.chessLogic = new ChessLogic({
          playsAs: this.gameService.playsAs(),
          onMadeMove: (moveId, encodedMove) => this.onMadeMove(moveId, encodedMove)
        });
        this.chessStatusSubscription = this.chessLogic.status.subscribe((chessStatus) => this.onChessStatusChanged(chessStatus));
      }
      this.chessLogic.updateMoves(gameData.moves);
      this.playTurnAudio();
      this.playVibrations();
    }
  }

  public getLowerBannerSide(): PlayerSide {
    const playsAs = this.gameService.playsAs();
    if (playsAs === PlayerSide.NONE) {
      return PlayerSide.WHITE;
    } else {
      return playsAs;
    }
  }

  public getUpperBannerSide(): PlayerSide {
    const playsAs = this.gameService.playsAs();
    if (playsAs === PlayerSide.NONE) {
      return PlayerSide.BLACK;
    } else {
      return BoardLayout.getOponentSide(playsAs);
    }
  }

  public onStartNewgame(): void {
    this.gameService.onCreateGame();
  }

}
