import { Coordinate } from '../models/coordinate';
import { Move } from '../models/move';

export abstract class FigureBase {
    public abstract getAllPossibleMoves(forField: Coordinate): Array<Move>;
    public abstract getAllAttackableFields(forField: Coordinate): Array<Coordinate>;
}
