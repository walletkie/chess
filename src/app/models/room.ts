import { PlayerStatus } from './player-status.enum';

export interface Room {
    players: {
        [uid: string]: PlayerStatus
    };
    game: string;
}

export function getNewRoom(uid: string): Room {
    return {
        players: {
            [uid]: PlayerStatus.OWNER
        },
        game: null
    };
}
