import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { faFlag } from '@fortawesome/free-regular-svg-icons';
import { faHandshake, faReplyAll } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';
import { ChessLogic } from 'src/app/chess-logic/chess-logic';
import { ChessFigure } from 'src/app/chess-logic/models/chess-figure.enum';
import { ChessStatus } from 'src/app/chess-logic/models/chess-status.enum';
import { PlayerSide } from 'src/app/chess-logic/models/player-side.enum';
import { Game } from 'src/app/models/game';
import { GameStatus } from 'src/app/models/game-status.enum';
import { AuthService } from 'src/app/services/auth.service';
import { GameService } from 'src/app/services/game.service';
import { PlayersListService } from 'src/app/services/players-list.service';
import { DisplayedStatus } from './displayed-status.enum';

@Component({
  selector: 'app-player-banner',
  templateUrl: './player-banner.component.html',
  styleUrls: ['./player-banner.component.scss']
})
export class PlayerBannerComponent implements OnInit, OnDestroy {

  public PlayerSide = PlayerSide;
  public ChessStatus = ChessStatus;
  public DisplayedStatus = DisplayedStatus;
  public GameStatus = GameStatus;

  public faHandshake = faHandshake;
  public faReplyAll = faReplyAll;
  public faFlag = faFlag;

  @Input()
  public chessLogic: ChessLogic;

  @Input()
  public side: PlayerSide;

  public playerUid: string;
  public displayedName: string;
  public avatarUrl: string;
  public beatenFiguresList: Array<ChessFigure> = [];
  public isThisPlayerBanner: boolean;
  public playsInThisGame: boolean;
  public displayedStatus: DisplayedStatus = DisplayedStatus.NONE;
  public result: GameStatus;

  private gameSubscription: Subscription;
  private beatenFiguresSubscription: Subscription;

  constructor(
    public playersListService: PlayersListService,
    public gameService: GameService,
    public authService: AuthService
  ) { }

  ngOnInit() {
    this.gameSubscription = this.gameService.game.subscribe((game) => this.onGameChange(game));
    if (this.side === PlayerSide.WHITE) {
      this.beatenFiguresSubscription =
        this.chessLogic.blackBeatenFigures.subscribe((beatenFiguresList) => this.onBeatenFiguresListChange(beatenFiguresList));
    } else if (this.side === PlayerSide.BLACK) {
      this.beatenFiguresSubscription =
        this.chessLogic.whiteBeatenFigures.subscribe((beatenFiguresList) => this.onBeatenFiguresListChange(beatenFiguresList));
    }
  }

  ngOnDestroy(): void {
    if (this.gameSubscription) {
      this.gameSubscription.unsubscribe();
    }
    if (this.beatenFiguresSubscription) {
      this.beatenFiguresSubscription.unsubscribe();
    }
  }

  private onGameChange(game: Game): void {
    if (!game) {
      return;
    }

    let newUid: string;
    if (this.side === PlayerSide.WHITE) {
      newUid = game.whitePlayerUid;
    } else if (this.side === PlayerSide.BLACK) {
      newUid = game.blackPlayerUid;
    }
    if (newUid !== this.playerUid) {
      this.playerUid = newUid;
    }
    if (this.result !== game.result) {
      this.result = game.result;
    }
    this.updatePlaysInThisGame();
    this.updateIsThisPlayerBanner();
    this.updateDisplayedStatus();
  }

  private onBeatenFiguresListChange(beatenFiguresList: Array<ChessFigure>): void {
    for (let indexOfFigureToAdd = this.beatenFiguresList.length;
      indexOfFigureToAdd < beatenFiguresList.length;
      indexOfFigureToAdd++) {
      this.beatenFiguresList.push(beatenFiguresList[indexOfFigureToAdd]);
    }
  }

  private updateTurnDisplayedStatus(): void {
    const chessStatus = this.chessLogic.status.getValue();
    if (chessStatus === ChessStatus.WHITE_TURN) {
      if (this.side === PlayerSide.WHITE) {
        if (this.chessLogic.underAttackBoard.whiteKingUnderAttack.getValue()) {
          this.displayedStatus = DisplayedStatus.IS_CHECKED;
        } else {
          this.displayedStatus = DisplayedStatus.TURN;
        }
      } else if (this.playsInThisGame) {
        this.displayedStatus = DisplayedStatus.AWAITS;
      }
    } else if (chessStatus === ChessStatus.BLACK_TURN) {
      if (this.side === PlayerSide.BLACK) {
        if (this.chessLogic.underAttackBoard.blackKingUnderAttack.getValue()) {
          this.displayedStatus = DisplayedStatus.IS_CHECKED;
        } else {
          this.displayedStatus = DisplayedStatus.TURN;
        }
      } else if (this.playsInThisGame) {
        this.displayedStatus = DisplayedStatus.AWAITS;
      }
    }
  }

  private updateDisplayedStatus(): void {
    this.displayedStatus = DisplayedStatus.NONE;

    if (this.result === GameStatus.UNDEFINED) {
      this.updateTurnDisplayedStatus();
    } else if (this.result === GameStatus.WHITE_WON) {
      if (this.side === PlayerSide.WHITE) {
        this.displayedStatus = DisplayedStatus.WON;
      } else {
        this.displayedStatus = DisplayedStatus.LOST;
      }
    } else if (this.result === GameStatus.BLACK_WON) {
      if (this.side === PlayerSide.WHITE) {
        this.displayedStatus = DisplayedStatus.LOST;
      } else {
        this.displayedStatus = DisplayedStatus.WON;
      }
    } else if (this.result === GameStatus.DRAW) {
      this.displayedStatus = DisplayedStatus.DRAW;
    } else if (this.result === GameStatus.WHITE_SURRENDERED) {
      if (this.side === PlayerSide.WHITE) {
        this.displayedStatus = DisplayedStatus.SURRENDERED;
      } else {
        this.displayedStatus = DisplayedStatus.WON;
      }
    } else if (this.result === GameStatus.BLACK_SURRENDERED) {
      if (this.side === PlayerSide.WHITE) {
        this.displayedStatus = DisplayedStatus.WON;
      } else {
        this.displayedStatus = DisplayedStatus.SURRENDERED;
      }
    }
  }

  private updatePlaysInThisGame(): void {
    this.playsInThisGame = this.gameService.playsInThisGame();
  }

  private updateIsThisPlayerBanner(): void {
    this.isThisPlayerBanner = this.gameService.playsAs() === this.side;
  }

  public onSurrender(): void {
    this.gameService.onSurrender();
  }

  public onOfferDraw(): void {
    this.gameService.onOfferDraw(true);
  }

  public onAcceptDraw(): void {
    this.gameService.onOfferDraw(true);
  }

  public onCancelOfferingDraw(): void {
    this.gameService.onOfferDraw(false);
  }

}
