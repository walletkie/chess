import { BoardLayout, Row } from '../board-layout';
import { ChessLogic } from '../chess-logic';
import { ChessFigure } from '../models/chess-figure.enum';
import { Coordinate } from '../models/coordinate';
import { Figure } from '../models/figure';
import { Move } from '../models/move';
import { MoveType } from '../models/move-type.enum';
import { PlayerSide } from '../models/player-side.enum';
import { FigureBase } from './figure-base';

export class Pawn extends FigureBase {

    private static readonly DIRECTION_FORWARD = 1;
    private static readonly DIRECTION_BACKWARD = -1;

    constructor(
        private chessLogic: ChessLogic
    ) {
        super();
    }

    public static getPawnDirection(figure: Figure): number {
        if (figure.color === PlayerSide.WHITE) {
            return Pawn.DIRECTION_FORWARD;
        } else if (figure.color === PlayerSide.BLACK) {
            return Pawn.DIRECTION_BACKWARD;
        }
        return null;
    }

    public static getStartRowFor(figure: Figure): Row {
        if (figure.color === PlayerSide.WHITE) {
            return '2';
        } else if (figure.color === PlayerSide.BLACK) {
            return '7';
        }
        return null;
    }

    public static getPromotionRowFor(figure: Figure): Row {
        if (figure.color === PlayerSide.WHITE) {
            return '8';
        } else if (figure.color === PlayerSide.BLACK) {
            return '1';
        }
        return null;
    }

    public static isTwoFieldsForwardMove(move: Move): boolean {
        const startIndex = BoardLayout.rows.indexOf(move.from.row);
        const endIndex = BoardLayout.rows.indexOf(move.to.row);
        return Math.abs(startIndex - endIndex) === 2;
    }

    public getAllPossibleMoves(forField: Coordinate): Array<Move> {
        const allPossibleMoves: Array<Move> = [];
        const pawn = this.chessLogic.figuresBoard.get(forField);

        // Move
        const allMoveableFields = this.getAllPropablyPossibleToMoveFields(forField);
        for (const movedField of allMoveableFields) {
            allPossibleMoves.push({
                from: forField,
                to: movedField,
                type: MoveType.MOVE
            });
        }

        // Attack
        const allAttackableFields = this.getAllAttackableFields(forField);
        for (const attackedField of allAttackableFields) {
            if (this.chessLogic.figuresBoard.isObtainedByOponent(pawn.color, attackedField)) {
                allPossibleMoves.push({
                    from: forField,
                    to: attackedField,
                    type: MoveType.ATTACK
                });
            }
        }

        // En passant
        const allEnPassantable = this.getAllPropablyPossibleToEnPassantFields(forField);
        for (const enPassantField of allEnPassantable) {
            allPossibleMoves.push({
                from: forField,
                to: enPassantField,
                type: MoveType.EN_PASSANT
            });
        }

        // Update to promotion
        const promotionRow = Pawn.getPromotionRowFor(pawn);
        for (const move of allPossibleMoves) {
            if (move.to.row === promotionRow) {
                move.type = MoveType.PROMOTION;
            }
        }

        return allPossibleMoves.filter(
            (move) => this.chessLogic.underAttackBoard.willKingBeUnderAttackAfterMove(forField, move.to) === false
        );
    }

    public getAllAttackableFields(field: Coordinate): Array<Coordinate> {
        const attackableFields = [];
        const figure = this.chessLogic.figuresBoard.get(field);
        const direction = Pawn.getPawnDirection(figure);

        const leftSideRow = BoardLayout.getRowPosition(field.row, direction);
        const leftSideCol = BoardLayout.getColPosition(field.col, -1);
        if (leftSideRow && leftSideCol) {
            attackableFields.push({ row: leftSideRow, col: leftSideCol });
        }
        const rightSideRow = BoardLayout.getRowPosition(field.row, direction);
        const rightSideCol = BoardLayout.getColPosition(field.col, 1);
        if (rightSideRow && rightSideCol) {
            attackableFields.push({ row: rightSideRow, col: rightSideCol });
        }

        return attackableFields;
    }

    private getAllPropablyPossibleToMoveFields(forField: Coordinate): Array<Coordinate> {
        const allMoveableFields = [];
        const figure = this.chessLogic.figuresBoard.get(forField);
        const direction = Pawn.getPawnDirection(figure);

        const oneRowForward = BoardLayout.getRowPosition(forField.row, direction);
        const oneFieldForward: Coordinate = { row: oneRowForward, col: forField.col };
        if (this.chessLogic.figuresBoard.isEmpty(oneFieldForward)) {
            allMoveableFields.push(oneFieldForward);

            if (Pawn.getStartRowFor(figure) === forField.row) {
                const twoRowsForward = BoardLayout.getRowPosition(forField.row, direction * 2);
                const twoFieldsForward: Coordinate = { row: twoRowsForward, col: forField.col };
                if (this.chessLogic.figuresBoard.isEmpty(twoFieldsForward)) {
                    allMoveableFields.push(twoFieldsForward);
                }
            }
        }

        return allMoveableFields;
    }

    private getAllPropablyPossibleToEnPassantFields(forField: Coordinate): Array<Coordinate> {
        const allAttackableFields = this.getAllAttackableFields(forField);
        const propablyPossibleToEnPassantFields: Array<Coordinate> = [];

        for (const attack of allAttackableFields) {
            const possibleToEnPassant = this.chessLogic.fieldsHistoryBoard.get({
                row: forField.row,
                col: attack.col
            });
            if (possibleToEnPassant.enPassantable) {
                if (this.chessLogic.figuresBoard.isEmpty(attack)) {
                    propablyPossibleToEnPassantFields.push(attack);
                }
            }
        }

        return propablyPossibleToEnPassantFields;
    }

    public getMoveType(fromCoordinate: Coordinate, toCoordinate: Coordinate): MoveType {
        const selectedFigure = this.chessLogic.figuresBoard.get(fromCoordinate);
        const destinationField = this.chessLogic.figuresBoard.get(toCoordinate);

        const promotionRow = Pawn.getPromotionRowFor(selectedFigure);
        if (promotionRow === toCoordinate.row) {
            return MoveType.PROMOTION;
        } else if (destinationField.chessFigure !== ChessFigure.NONE) {
            return MoveType.ATTACK;
        } else {
            if (fromCoordinate.col !== toCoordinate.col) {
                return MoveType.EN_PASSANT;
            } else {
                return MoveType.MOVE;
            }
        }
    }

}
