import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { DefaultLanguage } from '../models/language.enum';
import { Player } from '../models/player';
import { User } from '../models/user';
import { AuthService } from './auth.service';

export interface Settings {
  sound: boolean;
  language: string;
  vibrations: boolean;
  highlightPossibleMoves: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  public readonly player = new BehaviorSubject<Player>(undefined);
  public readonly settings = new BehaviorSubject<Settings>(undefined);

  private playerSubscription: Subscription;
  private settingsSubscription: Subscription;

  constructor(
    private firestore: AngularFirestore,
    private authService: AuthService,
    private translate: TranslateService,
    private cookieService: CookieService,
  ) {
    this.authService.user.subscribe((user) => this.updatePlayerCreditials(user));
    this.authService.user.subscribe((user) => this.updatePlayerSettings(user));
    this.settings.subscribe((settings) => this.onSettingsChange(settings));
  }

  private onSettingsChange(settings: Settings): void {
    if (settings && settings.language !== this.translate.currentLang) {
      this.translate.use(settings.language);
    }
  }

  private getDefaultSettings(): Settings {
    return {
      sound: true,
      vibrations: true,
      language: this.cookieService.get('language') || DefaultLanguage,
      highlightPossibleMoves: true,
    };
  }

  private getDefaultPlayerCredits(): Player {
    const user = this.authService.user.getValue();
    return {
      displayedName: user.displayedName || null,
      avatarUrl: user.avatarUrl || null
    };
  }

  private updatePlayerCreditials(user: User): void {
    if (this.playerSubscription) {
      this.playerSubscription.unsubscribe();
    }

    if (user && user.isLoggedIn) {
      const playerDocument = this.firestore.collection('players').doc<Player>(user.uid);
      this.playerSubscription = playerDocument.valueChanges().subscribe((playerData) => {
        if (!playerData) {
          this.setPlayer(this.getDefaultPlayerCredits());
        } else {
          this.player.next(playerData);
        }
      });
    } else {
      this.player.next(undefined);
    }
  }

  private updatePlayerSettings(user: User): void {
    if (this.settingsSubscription) {
      this.settingsSubscription.unsubscribe();
    }

    if (user && user.isLoggedIn) {
      const settingsDocument = this.firestore.collection('settings').doc<Settings>(user.uid);
      this.settingsSubscription = settingsDocument.valueChanges().subscribe((settingsData) => {
        if (!settingsData) {
          this.setSettings(this.getDefaultSettings());
        } else {
          this.settings.next(settingsData);
        }
      });
    } else {
      this.settings.next(undefined);
    }
  }

  public getPlayer(uid: string): AngularFirestoreDocument<Player> {
    return this.firestore.collection('players').doc<Player>(uid);
  }

  public getSettings(uid: string): AngularFirestoreDocument<Settings> {
    return this.firestore.collection('settings').doc<Settings>(uid);
  }

  async setPlayer(player: Player): Promise<void> {
    const uid = this.authService.user.getValue().uid;
    const playerDocument = this.firestore.collection('players').doc<Player>(uid);
    await playerDocument.set(player);
  }

  async setSettings(settings: Settings): Promise<void> {
    const uid = this.authService.user.getValue().uid;
    const settingsDocument = this.firestore.collection('settings').doc<Settings>(uid);
    await settingsDocument.set(settings);
  }

  async updatePlayer(player: Partial<Player>): Promise<void> {
    const uid = this.authService.user.getValue().uid;
    const playerDocument = this.firestore.collection('players').doc<Player>(uid);
    await playerDocument.update(player);
  }

  async updateSettings(settings: Partial<Settings>): Promise<void> {
    const uid = this.authService.user.getValue().uid;
    const settingsDocument = this.firestore.collection('settings').doc<Settings>(uid);
    await settingsDocument.update(settings);
  }
}
