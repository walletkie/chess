import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp();

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript

async function _createRoom(roomId: string, uid: string, gameId: string): Promise<string> {
    const roomDocument = admin.firestore().doc(`/rooms/${roomId}`);
    await roomDocument.set({
        players: {
            [uid]: 0
        },
        game: gameId
    });
    return roomDocument.id;
}

async function _createGame(uid: string): Promise<string> {
    const gamesCollection = admin.firestore().collection(`games`);
    const gameDocument = await gamesCollection.add({
        whitePlayerUid: uid,
        whitePlayerReady: false,
        blackPlayerUid: null,
        blackPlayerReady: false,
        status: 0
    });
    return gameDocument.id;
}

export const createRoom = functions.https.onCall(async (data, context) => {
    const roomId = data.roomId;

    if (!context.auth) {
        return {
            success: false, code: 401
        };
    }

    if (!roomId || context.auth.uid !== roomId) {
        return {
            success: false, code: 403
        };
    }

    const gameDocumentId = await _createGame(context.auth.uid);
    const roomDocumentId = await _createRoom(roomId, context.auth.uid, gameDocumentId);

    return {
        success: true, data: roomDocumentId
    };
});

export const createGame = functions.https.onCall(async (data, context) => {
    const roomId = data.roomId;

    if (!context.auth) {
        return {
            success: false, code: 401
        };
    }

    if (!roomId || context.auth.uid !== roomId) {
        return {
            success: false, code: 403
        };
    }

    const gameDocumentId = await _createGame(context.auth.uid);

    return {
        success: true, data: gameDocumentId
    };
});

