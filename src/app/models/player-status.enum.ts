export enum PlayerStatus {
    OWNER,
    ACTIVE,
    BANNED
}
