import { BoardLayout } from '../board-layout';
import { ChessLogic } from '../chess-logic';
import { ChessFigure } from '../models/chess-figure.enum';
import { Coordinate } from '../models/coordinate';
import { Move } from '../models/move';
import { MoveType } from '../models/move-type.enum';
import { FigureBase } from './figure-base';

export class Knight extends FigureBase {

    private static readonly AVAILABLE_MOVES_POSITIONS:
        Array<{ row: number, col: number }> = [
            { row: 1, col: 2 },
            { row: 1, col: -2 },
            { row: 2, col: 1 },
            { row: 2, col: -1 },
            { row: -1, col: 2 },
            { row: -1, col: -2 },
            { row: -2, col: 1 },
            { row: -2, col: -1 }
        ];

    constructor(
        private chessLogic: ChessLogic
    ) {
        super();
    }

    public getAllAttackableFields(forField: Coordinate): Array<Coordinate> {
        const allMoveableFields = [];
        for (const position of Knight.AVAILABLE_MOVES_POSITIONS) {
            const row = BoardLayout.getRowPosition(forField.row, position.row);
            const col = BoardLayout.getColPosition(forField.col, position.col);
            if (row && col) {
                allMoveableFields.push({ row, col });
            }
        }
        return allMoveableFields;
    }

    public getPossibleToAttackFields(forField: Coordinate): Array<Coordinate> {
        const possibleToAttackFields: Array<Coordinate> = [];
        const allAttackableFields = this.getAllAttackableFields(forField);
        for (const attack of allAttackableFields) {
            if (this.chessLogic.figuresBoard.isObtainedByOponent(this.chessLogic.playsAs, attack) &&
                !this.chessLogic.underAttackBoard.willKingBeUnderAttackAfterMove(forField, attack)) {
                possibleToAttackFields.push(attack);
            }
        }
        return possibleToAttackFields;
    }

    public getPossibleToMoveFields(forField: Coordinate): Array<Coordinate> {
        const possibleToMoveFields: Array<Coordinate> = [];
        const allAttackableFields = this.getAllAttackableFields(forField);
        for (const move of allAttackableFields) {
            if (this.chessLogic.figuresBoard.isEmpty(move) &&
                !this.chessLogic.underAttackBoard.willKingBeUnderAttackAfterMove(forField, move)) {
                possibleToMoveFields.push(move);
            }
        }
        return possibleToMoveFields;
    }

    public getAllPossibleMoves(forField: Coordinate): Array<Move> {
        const possibleMoves: Array<Move> = [];

        const possibleToMoveFields = this.getPossibleToMoveFields(forField);
        for (const move of possibleToMoveFields) {
            possibleMoves.push({
                from: forField,
                to: move,
                type: MoveType.MOVE
            });
        }

        const possibleToAttackFields = this.getPossibleToAttackFields(forField);
        for (const attack of possibleToAttackFields) {
            possibleMoves.push({
                from: forField,
                to: attack,
                type: MoveType.ATTACK
            });
        }

        return possibleMoves;
    }

    public getMoveType(toCoordinate: Coordinate): MoveType {
        const destinationField = this.chessLogic.figuresBoard.get(toCoordinate);
        if (destinationField.chessFigure !== ChessFigure.NONE) {
            return MoveType.ATTACK;
        } else {
            return MoveType.MOVE;
        }
    }

}
